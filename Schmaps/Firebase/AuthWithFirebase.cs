﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase;
using Firebase.Auth;

namespace Schmaps.Firebase
{
    class AuthWithFirebase
    {
        public static FirebaseApp app;
        public static FirebaseAuth auth;
        public static FirebaseAuth GetAuth()
        {
            if (app == null)
            {
                app = GetApp();
            }
            return FirebaseAuth.GetInstance(app);
        }
        public static FirebaseApp GetApp()
        {
            app = FirebaseApp.GetInstance(FirebaseApp.DefaultAppName);
            if (app == null)
            {
                var options = new FirebaseOptions.Builder()
                .SetApplicationId("schmaps")
                .SetApiKey("AIzaSyBXQznEHu1v29-BmJmmzM8f0EnWHVez9ac")
                .SetDatabaseUrl("https://schmaps.firebaseio.com")
                .SetStorageBucket("schmaps.appspot.com")
                .Build();

                return FirebaseApp.InitializeApp(Application.Context, options); ;
            }
            else
            {
                return app;
            }
        }
        public static Task<bool> SignInAnon()
        {
            if (auth == null)
            {
                auth = GetAuth();
            }
            auth.SignInAnonymously();
            return new Task<bool>(() => true);
        }

    }
}