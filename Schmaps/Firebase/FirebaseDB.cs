﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Widget;
using Firebase.Database;
using Firebase;
using Schmaps.Models;
using Schmaps.Resources.fragments;
using Android.Hardware;
using System.Runtime.CompilerServices;
using Android.Gms.Tasks;
using Xamarin.Essentials;
using Java.Interop;
using Java.Util;
using Android.Content;
using Android.Gms.Common.Apis;
using Schmaps.Firebase;
using Schmaps.Controller;

namespace Schmaps.Firebase
{
    public class FirebaseDB
    {
        #region Update Events
        public void OnMessageUpdate(MessageEventArgs e)
        {
            messageUpdate?.Invoke(null, e);
        }
        public event EventHandler<MessageEventArgs> messageUpdate = delegate { };
        public void OnPinUpdate(EventArgs e)
        {
            pinUpdate?.Invoke(null, EventArgs.Empty);
        }
        public event EventHandler pinUpdate = delegate { };
        public void OnConversationUpdate(EventArgs e)
        {
            conversationUpdate?.Invoke(null, EventArgs.Empty);
        }
        public event EventHandler conversationUpdate = delegate { };
        public void OnUserUpdate(EventArgs e)
        {
            userUpdate?.Invoke(null, EventArgs.Empty);
        }
        public event EventHandler userUpdate = delegate { };
        #endregion

        #region Fields & Properties
        public AppDB appDB;
        public FirebaseApp app;

        public UserEventListener usrEVL;
        private ConversationEventListener convoEVL;
        private MessageEventListener msgEVL;
        private PinEventListener pinEVL;
        public MainActivity Main { get; }

        public void OnInitFinished(EventArgs e)
        {
            InitFinished?.Invoke(null, EventArgs.Empty);
        }
        public event EventHandler InitFinished = delegate { };
        #endregion

        #region Constructor & Initiation
        public FirebaseDB(MainActivity main)
        {
            Main = main;
            messageUpdate += AddMessageToChat;
        }

        public void InitiateAndSetListeners()
        {
            if (appDB == null)
            {
                appDB = new AppDB
                {
                    pin = new List<Pin>(),
                    message = new List<Models.Message>(),
                    databaseUsers = new List<DatabaseUser>()
                };
            }

            if (AuthWithFirebase.auth.CurrentUser == null)
            {
                AuthWithFirebase.auth.SignInAnonymously().AddOnCompleteListener(new OnCompleteListener(this));
            }
            else
            {
                ////todo delete, exists because android keeps token even after app uninstall, user is deleted from firebase auth-db
                //if (AuthWithFirebase.auth.CurrentUser.Uid == "bmcOOkfOJhaebYv5WAmLRHgim2G3" || AuthWithFirebase.auth.CurrentUser.Uid == "K5DD8J2ijcUZdoJFT0EZf8C2htm1")
                //{
                //    AuthWithFirebase.auth.SignOut();
                //    AuthWithFirebase.app.Delete();
                //    AuthWithFirebase.auth.SignInAnonymously();
                //}

                var app = GetDatabase();
                GetDatabase().GetReference("AppDB").Child("user").AddListenerForSingleValueEvent(new UserCountListener(this));
                convoEVL = new ConversationEventListener(this);
                app.GetReference("AppDB").Child("conversation").AddChildEventListener(convoEVL);
                msgEVL = new MessageEventListener(this);
                app.GetReference("AppDB").Child("message").AddChildEventListener(msgEVL);
                pinEVL = new PinEventListener(this);
                app.GetReference("AppDB").Child("pin").AddChildEventListener(pinEVL);
                usrEVL = new UserEventListener(this);
                app.GetReference("AppDB").Child("user").AddChildEventListener(usrEVL);

                OnInitFinished(EventArgs.Empty);
            }
        }

        internal void SaveUser(DatabaseUser user)
        {
            DatabaseReference reference;
            HashMap mUser = new HashMap();
            mUser.Put("icon", Convert.ToString(user.icon));
            mUser.Put("name", Convert.ToString(user.name));
            mUser.Put("uid", user.uid);
            mUser.Put("lastSignedIn", user.lastSignedIn);
            mUser.Put("pubKey", user.publicKey);
            HashMap pendingRequests = new HashMap();
            int requestNumber = 0;
            if (user.pendingConversationRequests != null)
            {
                foreach (string uid in user.pendingConversationRequests)
                {
                    HashMap request = new HashMap();
                    request.Put("uid", uid);
                    pendingRequests.Put(Convert.ToString(requestNumber), request);
                    requestNumber++;
                }
                mUser.Put("pendingRequests", pendingRequests);
            }
            if (user.firebaseID != null)
            {
                reference = GetDatabase().GetReference("AppDB").Child("user").Child(appDB.appUser.firebaseID);

                reference.UpdateChildren(HashMapToDict(mUser));
            }
        }

        internal void SignOut()
        {
            AuthWithFirebase.auth.SignOut();
            AuthWithFirebase.app.Delete();
            AuthWithFirebase.auth.SignInAnonymously();
        }

        internal class OnCompleteListener : Java.Lang.Object, IOnCompleteListener
        {
            private FirebaseDB firebaseDB;

            public OnCompleteListener(FirebaseDB db)
            {
                firebaseDB = db;
            }

            public void OnComplete(Task task)
            {
                firebaseDB.GetDatabase().GetReference("AppDB").Child("user").AddListenerForSingleValueEvent(new UserCountListener(firebaseDB));
                firebaseDB.convoEVL = new ConversationEventListener(firebaseDB);
                firebaseDB.GetDatabase().GetReference("AppDB").Child("conversation").AddChildEventListener(firebaseDB.convoEVL);
                firebaseDB.msgEVL = new MessageEventListener(firebaseDB);
                firebaseDB.GetDatabase().GetReference("AppDB").Child("message").AddChildEventListener(firebaseDB.msgEVL);
                firebaseDB.pinEVL = new PinEventListener(firebaseDB);
                firebaseDB.GetDatabase().GetReference("AppDB").Child("pin").AddChildEventListener(firebaseDB.pinEVL);
                firebaseDB.usrEVL = new UserEventListener(firebaseDB);
                firebaseDB.GetDatabase().GetReference("AppDB").Child("user").AddChildEventListener(firebaseDB.usrEVL);

                firebaseDB.OnInitFinished(EventArgs.Empty);
            }
        }
        #endregion
        public FirebaseDatabase GetDatabase()
        {
            FirebaseDatabase database;
            if (app == null)
            {
                app = AuthWithFirebase.GetApp();
                database = FirebaseDatabase.GetInstance(app);
            }
            else
            {
                database = FirebaseDatabase.GetInstance(app);
            }
            return database;

        }
        #region Save To Firebase Realtime Database Methods

        public void RegisterUser(DatabaseUser dbUser)
        {
            HashMap mUser = new HashMap();
            if (String.IsNullOrEmpty(dbUser.icon))
            {
                mUser.Put("icon", "👤");
            }
            else
            {
                mUser.Put("icon", Convert.ToString(dbUser.icon));
            }
            mUser.Put("name", Convert.ToString(dbUser.name));
            mUser.Put("uid", dbUser.uid);
            mUser.Put("pubKey", dbUser.publicKey);
            mUser.Put("lastSignedIn", dbUser.lastSignedIn);
            HashMap pendingRequests = new HashMap();
            int requestNumber = 0;
            if (dbUser.pendingConversationRequests != null)
            {
                foreach (string uid in dbUser.pendingConversationRequests)
                {
                    HashMap request = new HashMap();
                    request.Put("uid", uid);
                    pendingRequests.Put(Convert.ToString(requestNumber), request);
                    requestNumber++;
                }
                mUser.Put("pendingRequests", pendingRequests);
            }
            // TODO check firebase, how many users really exist
            GetDatabase().GetReference("AppDB").Child("user").AddListenerForSingleValueEvent(new UserCountListener(this));

            DatabaseReference reference = GetDatabase().GetReference("AppDB").Child("user").Push();
            dbUser.firebaseID = reference.Key;
            reference.SetValue(mUser);
            appDB.databaseUsers.Add(dbUser);
            if (dbUser.uid == AuthWithFirebase.auth.Uid)
            {
                appDB.AppUser = dbUser;
            }

        }

        internal void StartConversation(string uid)
        {

            string uIdOfFriend = uid;

            int timestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string convoGuid = Guid.NewGuid().ToString();
            HashMap mConversation = new HashMap();
            HashMap members = new HashMap();
            members.Put("0", uIdOfFriend);
            members.Put("1", AuthWithFirebase.auth.Uid);
            mConversation.Put("members", members);
            mConversation.Put("conversationId", convoGuid);
            mConversation.Put("lastMessage", "none");
            mConversation.Put("timestamp", timestamp);

            DatabaseReference reference = GetDatabase().GetReference("AppDB").Child("conversation").Push();
            reference.SetValue(mConversation);
        }
        internal void SavePin(Pin pin)
        {
            HashMap mPin = new HashMap();
            mPin.Put("lat", Convert.ToString(pin.lat));
            mPin.Put("lon", Convert.ToString(pin.lon));
            mPin.Put("name", pin.name);
            mPin.Put("text", pin.text);
            HashMap mPinMembers = new HashMap();
            int memberNumber = 0;
            foreach (PinMember member in pin.members)
            {
                HashMap Member = new HashMap();
                Member.Put("uid", member.memberUid);
                if (member.isCreator)
                {
                    Member.Put("isCreator", true);
                }
                else
                {
                    Member.Put("isCreator", false);
                }
                mPinMembers.Put(Convert.ToString(memberNumber), Member);
                memberNumber++;
            }
            mPin.Put("members", mPinMembers);
            DatabaseReference reference;
            if (pin.firebaseID != null)
            {
                reference = GetDatabase().GetReference("AppDB").Child("pin").Child(pin.firebaseID);

                reference.UpdateChildren(HashMapToDict(mPin));
            }
            else
            {
                reference = GetDatabase().GetReference("AppDB").Child("pin").Push();
                pin.firebaseID = reference.Key;
                reference.SetValue(mPin);
            }
            if (appDB.pin.Exists(x => x.isUnfinished == true))
            {
                appDB.pin.RemoveAll(x => x.isUnfinished == true);
            }
        }
        internal void ChangePinMembers(Pin pin, string sharewithuid)
        {
            PinMember newMember = new PinMember();
            newMember.isCreator = false;
            newMember.memberUid = sharewithuid;
            if (pin.members.Exists(x => x.isCreator == newMember.isCreator && x.memberUid == newMember.memberUid))
            {
                pin.members.RemoveAll(x => x.memberUid == newMember.memberUid);
            }
            else
            {
                pin.members.Add(newMember);
            }
            SavePin(pin);
        }
        internal void DeletePin(Pin pin)
        {
            if (pin.members.Exists(x => x.isCreator == false && x.memberUid == AuthWithFirebase.auth.Uid))
            {
                DeleteMyselfFromPin(pin);
            }
            else if (pin.firebaseID != null)
            {
                DatabaseReference reference = GetDatabase().GetReference("AppDB").Child("pin").Child(pin.firebaseID);
                reference.RemoveValue();
                appDB.pin.Remove(pin);
            }
            else
            {
                Toast.MakeText(Application.Context, "Fehler beim Löschen des Pins", ToastLength.Short).Show();
            }
        }

        private void DeleteMyselfFromPin(Pin pin)
        {
            if (pin.firebaseID != null)
            {
                pin.members.RemoveAll(x => x.memberUid == AuthWithFirebase.auth.Uid);
                SavePin(pin);
            }
            else
            {
                Toast.MakeText(Application.Context, "Fehler beim Löschen des Pins", ToastLength.Short).Show();
            }
        }
        #endregion

        #region Firebase Realtime Database Event Listeners
        public class MessageEventListener : Java.Lang.Object, IChildEventListener
        {
            private MessageController handler;

            public MessageEventListener(FirebaseDB firebaseDB)
            {
                handler = new MessageController(firebaseDB);
            }

            public void OnCancelled(DatabaseError error)
            {
                Toast.MakeText(Application.Context, "(Message) Firebase Error : " + error, ToastLength.Short).Show();
            }

            public void OnChildAdded(DataSnapshot snapshot, string previousChildName)
            {
                handler.AddMessage(snapshot, previousChildName);
            }

            public void OnChildChanged(DataSnapshot snapshot, string previousChildName)
            {
                // No plans on changing existing Messages
                return;
            }

            public void OnChildMoved(DataSnapshot snapshot, string previousChildName)
            {
                // No plans on moving existing Messages
                return;
            }

            public void OnChildRemoved(DataSnapshot snapshot)
            {
                handler.RemoveMessage(snapshot);
            }
        }

        public class PinEventListener : Java.Lang.Object, IChildEventListener
        {
            private PinController handler;
            public PinEventListener(FirebaseDB firebaseDB)
            {
                handler = new PinController(firebaseDB);
            }

            public void OnCancelled(DatabaseError error)
            {
                Toast.MakeText(Application.Context, "(Pin) Firebase Error : " + error, ToastLength.Short).Show();
            }

            public void OnChildAdded(DataSnapshot snapshot, string previousChildName)
            {
                handler.AddPin(snapshot, previousChildName);
            }

            public void OnChildChanged(DataSnapshot snapshot, string previousChildName)
            {
                handler.ChangePin(snapshot, previousChildName);
            }

            public void OnChildMoved(DataSnapshot snapshot, string previousChildName)
            {
                //No plans on moving existing pins
            }

            public void OnChildRemoved(DataSnapshot snapshot)
            {
                handler.RemovePin(snapshot);
            }
        }


        public class ConversationEventListener : Java.Lang.Object, IChildEventListener
        {
            private ConversationController handler;
            public ConversationEventListener(FirebaseDB firebaseDB)
            {
                handler = new ConversationController(firebaseDB);
            }

            public void OnCancelled(DatabaseError error)
            {
                Toast.MakeText(Application.Context, "(Conversation) Firebase Error : " + error, ToastLength.Short).Show();
            }

            public void OnChildAdded(DataSnapshot snapshot, string previousChildName)
            {
                handler.AddConversation(snapshot, previousChildName);
                //Toast.MakeText(Application.Context, "(Conversation added)", ToastLength.Short).Show();
            }

            public void OnChildChanged(DataSnapshot snapshot, string previousChildName)
            {
                handler.ChangeConversation(snapshot, previousChildName);
            }

            public void OnChildMoved(DataSnapshot snapshot, string previousChildName)
            {
                //No plans on moving existing conversations
            }

            public void OnChildRemoved(DataSnapshot snapshot)
            {
                handler.RemoveConversation(snapshot);
            }
        }
        public class UserEventListener : Java.Lang.Object, IChildEventListener
        {
            public UserController handler;
            public UserEventListener(FirebaseDB firebaseDB)
            {
                handler = new UserController(firebaseDB);
            }

            public void OnCancelled(DatabaseError error)
            {
                AuthWithFirebase.auth.SignOut();
                AuthWithFirebase.app.Delete();
                AuthWithFirebase.auth.SignInAnonymously();
                Toast.MakeText(Application.Context, "(User) Firebase Error : " + error, ToastLength.Short).Show();
            }

            public void OnChildAdded(DataSnapshot snapshot, string previousChildName)
            {
                handler.AddUser(snapshot, previousChildName);
                //Toast.MakeText(Application.Context, "(User added)", ToastLength.Short).Show();
            }

            public void OnChildChanged(DataSnapshot snapshot, string previousChildName)
            {
                handler.ChangeUser(snapshot, previousChildName);
            }

            public void OnChildMoved(DataSnapshot snapshot, string previousChildName)
            {
                //No plans on moving existing users
            }

            public void OnChildRemoved(DataSnapshot snapshot)
            {
                handler.RemoveUser(snapshot);
            }
        }

        public class UserCountListener : Java.Lang.Object, IValueEventListener
        {
            private FirebaseDB firebaseDB;

            public UserCountListener(FirebaseDB firebaseDB)
            {
                this.firebaseDB = firebaseDB;
            }

            public void OnCancelled(DatabaseError error)
            {
                throw new NotImplementedException();
            }

            public void OnDataChange(DataSnapshot snapshot)
            {
                firebaseDB.appDB.userCount = (int)snapshot.ChildrenCount;
                firebaseDB.OnUserUpdate(EventArgs.Empty);
            }
        }

        #endregion

        #region Helper Methods
        internal DatabaseUser FindUserByUID(string member)
        {
            return appDB.databaseUsers.Find(x => x.uid == member);
        }

        internal DatabaseUser FindUserByFirebaseID(string member)
        {
            return appDB.databaseUsers.Find(x => x.firebaseID == member);
        }

        internal Conversation FindConversationByFirebaseID(string convo)
        {
            return appDB.Conversations.Find(x => x.firebaseID == convo);
        }

        internal Pin FindPinByFirebaseID(string pin)
        {
            return appDB.pin.Find(x => x.firebaseID == pin);
        }

        internal Message FindMessageByFirebaseID(string msg)
        {
            return appDB.message.Find(x => x.firebaseID == msg);
        }
        internal string FindConversationWithUsers(string myUid, string chatPartnerUid)
        {
            string conversationID = String.Empty;
            foreach (Conversation convo in appDB.Conversations)
            {
                if (convo.members.Exists(x => x == myUid) && convo.members.Exists(y => y == chatPartnerUid))
                {
                    conversationID = convo.conversationId;
                    break;
                }
            }
            return conversationID;
        }
        public bool UserMatchesAppUser(string user)
        {
            if (user == AuthWithFirebase.auth.Uid)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool AppUserIsRegistered()
        {
            if (appDB.databaseUsers.Exists(x => x.uid == AuthWithFirebase.auth.Uid))
            {
                return true;
            }
            return false;
        }
        internal MainActivity GetMainActivity()
        {
            return appDB.main;
        }
        internal List<DatabaseUser> PrepareFriendsList()
        {
            if (appDB.ListOfFriends == null)
            {
                appDB.ListOfFriends = new List<string>();
            }
            if (appDB.Conversations != null)
            {
                List<Conversation> convos = appDB.Conversations;
                foreach (Conversation conversation in convos)
                {
                    if (conversation.members.Contains(AuthWithFirebase.auth.Uid))
                    {
                        foreach (string member in conversation.members)
                        {
                            if (member != AuthWithFirebase.auth.Uid)
                            {
                                if (!appDB.ListOfFriends.Contains(member))
                                {
                                    appDB.ListOfFriends.Add(member);
                                }
                            }
                        }
                    }
                }
            }
            List<DatabaseUser> users = new List<DatabaseUser>();
            foreach (string userUid in appDB.ListOfFriends)
            {
                DatabaseUser databaseUser = FindUserByUID(userUid);
                if (databaseUser != null)
                {
                    users.Add(databaseUser);
                }
            }
            return users;
        }
        internal List<Message> GetChats(string user)
        {
            if (appDB.Chats == null)
            { PrepareChats(); }

            string convoId = FindConversationWithUsers(AuthWithFirebase.auth.Uid, user);
            List<Message> myChat = appDB.Chats[convoId];
            return myChat;
        }

        private void AddMessageToChat(object sender, MessageEventArgs e)
        {
            if (e.ConvoID != null && e.MsgID != null && appDB.Chats != null && !appDB.Chats[e.ConvoID].Exists(x => x.firebaseID == e.MsgID))
            {
                Message toAdd = FindMessageByFirebaseID(e.MsgID);
                appDB.Chats[e.ConvoID].Add(toAdd);
            }
        }

        internal void PrepareChats()
        {
            if (appDB.Chats == null)
            {
                appDB.Chats = new Dictionary<string, List<Message>>();
                foreach (var item in appDB.Conversations)
                {
                    List<Message> chat = new List<Message>();
                    chat = appDB.message.FindAll(x => x.conversationId == item.conversationId);

                    appDB.message.RemoveAll(x => !appDB.Conversations.Exists(y => x.conversationId == y.conversationId));
                    appDB.Chats.Add(item.conversationId, chat);
                }
            }
        }

        internal Dictionary<string, Java.Lang.Object> HashMapToDict(HashMap map)
        {
            Dictionary<string, Java.Lang.Object> keyValuePairs = new Dictionary<string, Java.Lang.Object>();
            var iterator = map.KeySet().GetEnumerator();
            while (iterator.MoveNext())
            {
                keyValuePairs.Add(iterator.Current.ToString(), map.Get((Java.Lang.Object)iterator.Current));
            }
            return keyValuePairs;
        }
        #endregion

    }
}
