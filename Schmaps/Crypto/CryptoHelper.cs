﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using System.Security.Cryptography;
using Xamarin.Essentials;
using System.Threading.Tasks;

namespace Schmaps.Crypto
{
    class CryptoHelper
    {
        public static string CreateKeys()
        {
            //create new CSP with a new 2048 bit rsa key pair
            var csp = new RSACryptoServiceProvider(2048);

            //get the private key
            var privKey = csp.ExportParameters(true);

            StorePrivateKey(KeyToString(privKey));
            //get public key ...
            var pubKey = csp.ExportParameters(false);
            return KeyToString(pubKey);
        }

        public static string Encrypt(string toEncrypt, string pubKeyString)
        {
            RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
            csp.ImportParameters(StringToKey(pubKeyString));

            var plainTextData = toEncrypt;
            var bytesPlainTextData = System.Text.Encoding.Unicode.GetBytes(plainTextData);
            var bytesCypherText = csp.Encrypt(bytesPlainTextData, true);

            return Convert.ToBase64String(bytesCypherText);
        }

        private static RSAParameters StringToKey(string keyString)
        {
            var sr = new System.IO.StringReader(keyString);
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            return (RSAParameters)xs.Deserialize(sr);
        }
        internal static string KeyToString(RSAParameters key)
        {
            var sw = new System.IO.StringWriter();
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            xs.Serialize(sw, key);
            return sw.ToString();
        }

        internal static async void StorePrivateKey(string keyToStore)
        {
            try
            {
                //SecureStorage.Remove("MyPrivateKey");
                string test = await SecureStorage.GetAsync("MyPrivateKey");
                if (test == null)
                {
                    await SecureStorage.SetAsync("MyPrivateKey", keyToStore);
                }
            }
            catch (Exception ex)
            {
                // Possible that device doesn't support secure storage on device.
            }
        }
        internal static async Task<string> RetrievePrivateKey()
        {
            string privateKey = null;
            try
            {
                privateKey = await SecureStorage.GetAsync("MyPrivateKey");
            }
            catch (Exception ex)
            {
                // Possible that device doesn't support secure storage on device.
            }

            return privateKey;
        }
        public static async Task<string> Decrypt(string toDecrypt)
        {
            byte[] bytesCypherText = Convert.FromBase64String(toDecrypt);

            //get csp and load private key
            RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
            csp.ImportParameters(StringToKey(await RetrievePrivateKey()));

            //decrypt and strip padding
            byte[] bytesPlainTextData = csp.Decrypt(bytesCypherText, true);
            string result = System.Text.Encoding.Unicode.GetString(bytesPlainTextData);
            return result;
        }
    }
}