﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace Schmaps
{
    [Activity(Label = "SettingsActivity", WindowSoftInputMode = SoftInput.AdjustResize, ScreenOrientation = ScreenOrientation.Portrait)]
    public class SettingsActivity : AppCompatActivity
    {
        EditText iconET;
        TextView previewIconET;
        EditText nameET;
        TextView previewNameET;
        Button saveButton;
        ViewGroup previewCardView;
        Button signOutButton;
        Intent main;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_settings);
            iconET = FindViewById<EditText>(Resource.Id.userIconET);
            nameET = FindViewById<EditText>(Resource.Id.userNameET);
            previewCardView = FindViewById<ViewGroup>(Resource.Id.previewFLItem);
            signOutButton = FindViewById<Button>(Resource.Id.signOutButton);
            saveButton = FindViewById<Button>(Resource.Id.saveButton);
            previewIconET = previewCardView.FindViewById<TextView>(Resource.Id.userIconTextView);
            previewNameET = previewCardView.FindViewById<TextView>(Resource.Id.usernameTextView);
            iconET.TextChanged += IconET_TextChanged;
            nameET.TextChanged += NameET_TextChanged;
            saveButton.Click += SaveButton_Click;
            signOutButton.Click += SignOutButton_Click;
            main = new Intent(this, typeof(MainActivity));

            string icon = Intent.GetStringExtra("icon");
            string name = Intent.GetStringExtra("name");
            previewIconET.Text = icon;
            previewNameET.Text = name;
        }

        private void SignOutButton_Click(object sender, EventArgs e)
        {
            Toast.MakeText(Application.Context, "Beim ''Speichern'' und Öffnen der MainActivity wird eine neue ID abgerufen. Hast du versehentlich gedrückt, schließe die App oder gehe 'Zurück'", ToastLength.Short).Show();
            main.PutExtra("signOut", true);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            main.PutExtra("changeUser", true);
            main.PutExtra("lastSignedIn", (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
            main.PutExtra("name", nameET.Text);
            main.PutExtra("icon", iconET.Text);
            main.AddFlags(ActivityFlags.ReorderToFront);
            StartActivity(main);
            OverridePendingTransition(Resource.Animation.abc_fade_in, Resource.Animation.abc_fade_out);
        }

        private void NameET_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            previewNameET.Text = nameET.Text;
        }

        private void IconET_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            previewIconET.Text = iconET.Text;
        }
    }
}