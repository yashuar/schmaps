﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Firebase.Database;
using Java.Util;
using Schmaps.Firebase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Essentials;

namespace Schmaps.Controller
{
    public class ChatController
    {
        internal static void HandleChatButtonClick(MainActivity mainActivity, EditText chatText, Bundle arguments, string chatPartnerPubKey)
        {
            Models.Message localMsg = new Models.Message();
            HashMap mMessage = new HashMap();

            int timestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            localMsg.timestamp = timestamp;
            mMessage.Put("timestamp", timestamp);


            localMsg.sender = AuthWithFirebase.auth.Uid;
            mMessage.Put("sender", AuthWithFirebase.auth.Uid);

            string conversationID = mainActivity.firebaseDB.FindConversationWithUsers(AuthWithFirebase.auth.Uid, arguments.GetString("user"));
            localMsg.conversationId = conversationID;
            mMessage.Put("conversationId", conversationID);

            localMsg.messageText = chatText.Text;
            string MessageText = Crypto.CryptoHelper.Encrypt(chatText.Text, chatPartnerPubKey);
            mMessage.Put("messageText", MessageText);

            chatText.Text = "";
            InputMethodManager imm = (InputMethodManager)mainActivity.BaseContext.GetSystemService(Context.InputMethodService);
            imm.HideSoftInputFromWindow(chatText.WindowToken, 0);

            DatabaseReference reference = mainActivity.firebaseDB.GetDatabase().GetReference("AppDB").Child("message").Push();
            localMsg.firebaseID = reference.Key;
            reference.SetValue(mMessage);

            mainActivity.firebaseDB.appDB.message.Add(localMsg);
            SecureStorage.SetAsync(localMsg.firebaseID, localMsg.messageText);
            mainActivity.firebaseDB.OnMessageUpdate(new MessageEventArgs(localMsg.conversationId, localMsg.firebaseID));
        }
    }
    public class ChatAdapter : RecyclerView.Adapter
    {
        List<Models.Message> mChatMessages;
        FirebaseDB firebaseDB;
        string user;
        RecyclerView RChatView;
        public ChatAdapter(FirebaseDB pFirebaseDB, string pUser, RecyclerView rChatView)
        {
            user = pUser;
            firebaseDB = pFirebaseDB;
            mChatMessages = firebaseDB.GetChats(user);
            RChatView = rChatView;
            firebaseDB.messageUpdate += UpdateData;

            if (mChatMessages.Count == 0)
            {
                Models.Message emptyMessage = new Models.Message();
                emptyMessage.messageText = "Eure Nachrichten sind jetzt Ende-zu-Ende verschlüsselt. Verlierst du den Zugriff auf deinen Account, kannst du sie leider nicht mehr lesen.";
                mChatMessages.Add(emptyMessage);
            }
        }
        public override int ItemCount => mChatMessages.Count();

        public void UpdateData(object sender, EventArgs e)
        {
            mChatMessages = firebaseDB.GetChats(user);
            this.NotifyDataSetChanged();
            this.RChatView.SmoothScrollToPosition(mChatMessages.Count - 1);
        }
        public override int GetItemViewType(int position)
        {
            if (mChatMessages[position].sender == AuthWithFirebase.auth.Uid)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = new View(parent.Context);
            switch (viewType)
            {
                case 0:
                    itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.chatMessage, parent, false);
                    break;
                case 1:
                    itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.chatMessage_own, parent, false);
                    break;
                default:
                    break;
            }

            ChatMessageViewHolder vh = new ChatMessageViewHolder(itemView);
            return vh;
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            ChatMessageViewHolder vh = holder as ChatMessageViewHolder;
            if (mChatMessages[position].sender == AuthWithFirebase.auth.Uid)
            {
                vh.ChatMessage.Text = SecureStorage.GetAsync(mChatMessages[position].firebaseID).Result;
            }
            else if (mChatMessages[position].sender == null)
            {
                vh.ChatMessage.Text = mChatMessages[position].messageText;
            }
            else
            {
                vh.ChatMessage.Text = Crypto.CryptoHelper.Decrypt(mChatMessages[position].messageText).Result;
            }
            vh.Timestamp.Text = mChatMessages[position].humanReadableTime;
        }
    }
    public class ChatMessageViewHolder : RecyclerView.ViewHolder
    {
        public TextView ChatMessage { get; private set; }
        public TextView Timestamp { get; private set; }

        public ChatMessageViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
            ChatMessage = itemView.FindViewById<TextView>(Resource.Id.chatMessageText);
            Timestamp = itemView.FindViewById<TextView>(Resource.Id.timestampText);
        }
    }
}