﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Mapsui;
using Mapsui.Projection;
using Schmaps.Firebase;
using Schmaps.MapHelpers;
using Schmaps.Models;
using Schmaps.Resources.fragments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schmaps.Controller
{
    class MapController
    {
        internal static List<Pin> GetPinList(Resources.fragments.Map_frgmt map_frgmt)
        {
            return map_frgmt.mainActivity.firebaseDB.appDB.pin;
        }

        internal static void SetPinList(Map_frgmt map_frgmt, List<Pin> value)
        {
            map_frgmt.mainActivity.firebaseDB.appDB.pin = value;
        }

        internal static void OnCreate_HandleUnfinishedPins(Map_frgmt map_frgmt)
        {
            if (map_frgmt.mainActivity.firebaseDB.appDB.pin.Exists(x => x.isUnfinished == true))
            {
                map_frgmt.addPinIsActive = true;
            }
            else
            {
                map_frgmt.addPinIsActive = false;
            }
        }

        internal static void HandleSearchImageButtonClick(Map_frgmt map_frgmt, object sender, EventArgs e, string searchText)
        {
            map_frgmt.addPinIsActive = false;
            Root locations = LocationHelpers.GetListByAddress(searchText).Result;
            if (map_frgmt.pinList.Exists(x => x.isUnfinished == true))
            {
                map_frgmt.pinList.RemoveAll(unfinishedPin);
                foreach (var item in map_frgmt.mapcontrol.Map.Layers.FindLayer("Points"))
                {
                    map_frgmt.mapcontrol.Map.Layers.Remove(item);
                }
                map_frgmt.mapcontrol.RefreshData();
                map_frgmt.mapcontrol.Refresh();
            }
            Dictionary<string, Pin> keyValuePairs = new Dictionary<string, Pin>();
            foreach (MapHelpers.Result result in locations.results)
            {
                List<MapHelpers.Location> subResults = GetLocationsFromResults(result);
                keyValuePairs = GetDictionaryFromResults(subResults);
                foreach (KeyValuePair<string, Pin> kvp in keyValuePairs)
                {
                    map_frgmt.pinList.Add(kvp.Value);
                }
                if (keyValuePairs.Count > 1)
                {
                    Toast.MakeText(Android.App.Application.Context, "Mehrere Ergebnisse, bitte aus der Pin-Liste speichern.", ToastLength.Short).Show();
                }
                else
                {
                    string adress = result.locations[0].adminArea1 + ", " + result.locations[0].adminArea5 + ", " + result.locations[0].postalCode + ", " + result.locations[0].street;
                    Toast.MakeText(Android.App.Application.Context, adress, ToastLength.Short).Show();
                }
            }
            List<Pin> list = new List<Pin>(keyValuePairs.Values);
            map_frgmt.mapcontrol.Navigator.NavigateTo(LocationHelpers.FindBoundingBoxForListOfPins(list));
            //pinList.Add(new Pin("name", location.Latitude, location.Longitude, "text", new PinMember(true, AuthWithFirebase.auth.Uid), true));
            map_frgmt.mapcontrol.Map.Layers.Add(map_frgmt.CreatePointLayer());
            map_frgmt.mapcontrol.RefreshData();
            map_frgmt.mapcontrol.Refresh();
            //string adress = LocationHelpers.GetAdressForLatLong(location).Result;
            //var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(location.Longitude, location.Latitude);
            //mapcontrol.Navigator.NavigateTo(sphericalMercatorCoordinate, mapcontrol.Map.Resolutions[14]);
            //Toast.MakeText(Android.App.Application.Context, adress, ToastLength.Short).Show();
        }
        private static List<MapHelpers.Location> GetLocationsFromResults(MapHelpers.Result result)
        {
            return result.locations;
        }
        private static Dictionary<string, Pin> GetDictionaryFromResults(List<MapHelpers.Location> subResults)
        {
            Dictionary<string, Pin> keyValuePairs = new Dictionary<string, Pin>();
            foreach (MapHelpers.Location loc in subResults)
            {
                string adress = loc.adminArea1 + ", " + loc.adminArea5 + ", " + loc.postalCode + ", " + loc.street;
                Pin pin = new Pin();
                pin.isUnfinished = true;
                pin.lat = loc.latLng.lat;
                pin.lon = loc.latLng.lng;
                pin.name = "name";
                pin.text = "desc";
                pin.members = new List<PinMember>();
                pin.members.Add(new PinMember(true, AuthWithFirebase.auth.Uid));
                if (!keyValuePairs.ContainsKey(adress))
                {
                    keyValuePairs.Add(adress, pin);
                }
            }
            return keyValuePairs;
        }

        internal static bool HandleAddPinIsActive_FABCLick(Map_frgmt map_frgmt)
        {
            if (map_frgmt.pinList.Exists(x => x.isUnfinished == true))
            {
                map_frgmt.addPinIsActive = false;
                return true;
            }
            return false;
        }
        public static bool unfinishedPin(Pin obj)
        {
            if (obj.isUnfinished == true)
            {
                return true;
            }
            return false;
        }

        internal static void HandleAddPinIsActive_MapTap(object sender, Mapsui.UI.MapInfoEventArgs e, Map_frgmt map_frgmt)
        {
            Mapsui.Geometries.Point point = SphericalMercator.ToLonLat(e.MapInfo.WorldPosition.X, e.MapInfo.WorldPosition.Y);
            if (map_frgmt.pinList.Exists(x => x.isUnfinished == true))
            {
                map_frgmt.pinList.RemoveAll(unfinishedPin);
                foreach (var item in map_frgmt.mapcontrol.Map.Layers.FindLayer("Points"))
                {
                    map_frgmt.mapcontrol.Map.Layers.Remove(item);
                }
                map_frgmt.mapcontrol.RefreshData();
                map_frgmt.mapcontrol.Refresh();
            }
            string adress = LocationHelpers.GetAdressForLatLong(new LocationObject(point.Y, point.X, 0)).Result;

            map_frgmt.pinList.Add(new Pin("name", point.Y, point.X, "text", new PinMember(true, AuthWithFirebase.auth.Uid), true));
        }
    }
}