﻿using Firebase.Database;
using Schmaps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Schmaps.Firebase;
using Schmaps.MapHelpers;
using Schmaps.Resources.fragments;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Widget;
using Android.Views;

namespace Schmaps.Controller
{
    internal class PinController
    {
        private FirebaseDB firebaseDB;

        public PinController(FirebaseDB firebaseDB)
        {
            this.firebaseDB = firebaseDB;
        }

        internal void AddPin(DataSnapshot snapshot, string previousChildName)
        {
            if (firebaseDB.appDB.pin == null)
            {
                firebaseDB.appDB.pin = new List<Pin>();
            }

            Pin toAdd = DeserializePin(snapshot, previousChildName);
            if (!firebaseDB.appDB.pin.Contains(toAdd))
            {
                if (toAdd.members.Exists(x => x.memberUid == AuthWithFirebase.auth.Uid))
                {
                    firebaseDB.appDB.pin.Add(toAdd);
                    firebaseDB.OnPinUpdate(EventArgs.Empty);
                }
            }
        }

        internal static Pin GetPinForSharePin(SharePin_frgmt sharePin_frgmt, Bundle arguments)
        {
            return sharePin_frgmt.mainActivity.firebaseDB.appDB.pin[arguments.GetInt("position")];
        }

        internal void ChangePin(DataSnapshot snapshot, string previousChildName)
        {
            Pin oldPin = firebaseDB.FindPinByFirebaseID(snapshot.Key);
            Pin newPin = DeserializePin(snapshot, previousChildName);

            if (oldPin == null)
            {
                firebaseDB.appDB.pin.Add(newPin);
            }
            else
            {
                if (!newPin.members.Exists(x => x.memberUid == AuthWithFirebase.auth.Uid))
                {
                    firebaseDB.appDB.pin.Remove(oldPin);
                }
                oldPin = newPin;
            }

            firebaseDB.OnPinUpdate(EventArgs.Empty);
        }

        internal static Pin GetPinForViewPin(ViewPin_frgmt viewPin_frgmt, int pinPosition)
        {
            return viewPin_frgmt.mainActivity.firebaseDB.appDB.pin[pinPosition];
        }

        internal static void PrepareExamplePin(PinList_frgmt pinList_frgmt)
        {
            if (pinList_frgmt.mainActivity.firebaseDB.appDB.pin.Count == 0)
            {
                Pin example = new Pin();
                example.lat = 52.520820;
                example.lon = 13.409425;
                example.name = "Beispiel-Pin";
                example.text = "Der Berliner Fernsehturm. Du kannst diesen Pin jederzeit löschen.";
                example.members = new List<PinMember>();
                example.members.Add(new PinMember(true, AuthWithFirebase.auth.Uid));
                pinList_frgmt.mainActivity.firebaseDB.appDB.pin.Add(example);
            }
            pinList_frgmt.adapter = new PinAdapter(pinList_frgmt.mainActivity.firebaseDB.appDB.pin, pinList_frgmt.mainActivity);
        }

        internal static void HandleSharePin_ListItemClicked(SharePin_frgmt sharePin_frgmt, int e)
        {
            sharePin_frgmt.mainActivity.firebaseDB.ChangePinMembers(sharePin_frgmt.pin, (sharePin_frgmt.mainActivity.firebaseDB.PrepareFriendsList()[e].uid));
        }

        internal static void HandlePinListItemClicked(PinList_frgmt pinList_frgmt, object sender, int e)
        {
            //Toast.MakeText(Application.Context, "Pin " + mainActivity.firebaseDB.appDB.pin[e].name + " selected", ToastLength.Short).Show();
            ViewPin_frgmt frgmt = new ViewPin_frgmt();
            frgmt.Arguments = new Bundle();
            frgmt.Arguments.PutDouble("lat", pinList_frgmt.mainActivity.firebaseDB.appDB.pin[e].lat);
            frgmt.Arguments.PutDouble("lon", pinList_frgmt.mainActivity.firebaseDB.appDB.pin[e].lon);
            frgmt.Arguments.PutInt("position", e);

            bool isOwn = pinList_frgmt.mainActivity.firebaseDB.appDB.pin[e].members.Exists(x => x.isCreator == true && x.memberUid == AuthWithFirebase.auth.Uid);
            frgmt.Arguments.PutBoolean("own", isOwn);

            List<Android.Support.V4.App.Fragment> sharePinFrag = new List<Android.Support.V4.App.Fragment>() { frgmt };
            ((MainActivity)pinList_frgmt.Activity).ReplaceContent(pinList_frgmt, null, sharePinFrag, "sharePin");
        }

        internal static void HandleViewPinDeleteButtonClick(ViewPin_frgmt viewPin_frgmt, object sender, EventArgs e, Pin pin)
        {
            viewPin_frgmt.mainActivity.firebaseDB.DeletePin(pin);
            viewPin_frgmt.mainActivity.OnBackPressed();
        }

        internal static void HandleViewPinSaveButtonClick(ViewPin_frgmt viewPin_frgmt, object sender, EventArgs e)
        {
            viewPin_frgmt.mainActivity.firebaseDB.appDB.pin.RemoveAll(x => x.isUnfinished == true && x.lat != viewPin_frgmt.pin.lat && x.lon != viewPin_frgmt.pin.lon);
            viewPin_frgmt.mainActivity.ReplaceContent(viewPin_frgmt, null, new List<Android.Support.V4.App.Fragment>() { new PinDetail_frgmt() }, "pinDetail");
        }

        internal static void HandleViewPinEditPinButtonClick(ViewPin_frgmt viewPin_frgmt, object sender, EventArgs e)
        {
            viewPin_frgmt.mainActivity.firebaseDB.appDB.pin[viewPin_frgmt.Arguments.GetInt("position")].isUnfinished = true;
            viewPin_frgmt.mainActivity.ReplaceContent(viewPin_frgmt, null, new List<Android.Support.V4.App.Fragment>() { new PinDetail_frgmt() }, "pinDetail");
        }

        internal static string PrepareAdressForPinDetail(Pin unfinishedPin)
        {
            return LocationHelpers.GetAdressForLatLong(new LocationObject(unfinishedPin.lat, unfinishedPin.lon, 0)).Result;
        }

        internal static Pin FindUnfinishedPin(Resources.fragments.PinDetail_frgmt pinDetail_frgmt)
        {
            return pinDetail_frgmt.mainActivity.firebaseDB.appDB.pin.Find(p => p.isUnfinished == true);
        }

        internal void RemovePin(DataSnapshot snapshot)
        {
            firebaseDB.appDB.pin.Remove(firebaseDB.FindPinByFirebaseID(snapshot.Key));
            firebaseDB.OnPinUpdate(EventArgs.Empty);
        }


        private Pin DeserializePin(DataSnapshot snapshot, string previousChildName)
        {
            Pin toAdd = new Pin
            {
                members = new List<PinMember>()
            };

            IEnumerable<DataSnapshot> pinIterable = snapshot.Children.ToEnumerable<DataSnapshot>();

            toAdd.firebaseID = Convert.ToString(snapshot.Key);

            foreach (var item in pinIterable)
            {
                if (item.Key == "lat")
                {
                    toAdd.lat = Double.Parse(Convert.ToString(item.Value));
                }
                else if (item.Key == "lon")
                {
                    toAdd.lon = Double.Parse(Convert.ToString(item.Value));
                }
                else if (item.Key == "name")
                {
                    toAdd.name = Convert.ToString(item.Value);
                }
                else if (item.Key == "text")
                {
                    toAdd.text = Convert.ToString(item.Value);
                }
                else if (item.Key == "members")
                {
                    IEnumerable<DataSnapshot> membersIterable = item.Children.ToEnumerable<DataSnapshot>();
                    foreach (var members in membersIterable)
                    {
                        IEnumerable<DataSnapshot> thisMemberIterable = members.Children.ToEnumerable<DataSnapshot>();
                        PinMember toAddMember = new PinMember();
                        foreach (var memberItem in thisMemberIterable)
                        {
                            if (memberItem.Key == "isCreator")
                            {
                                string value = Convert.ToString(memberItem.Value);

                                if (value == "True")
                                {
                                    toAddMember.isCreator = true;
                                }
                                else
                                {
                                    toAddMember.isCreator = false;
                                }
                            }
                            else if (memberItem.Key == "uid")
                            {
                                toAddMember.memberUid = Convert.ToString(memberItem.Value);
                            }
                        }
                        toAdd.members.Add(toAddMember);
                    }
                }
            }

            return toAdd;
        }

        internal static void HandlePinDetailSaveButtonClick(PinDetail_frgmt pinDetail_frgmt, object sender, EventArgs e)
        {
            if (pinDetail_frgmt.unfinishedPin != null)
            {
                LocationObject location = LocationHelpers.GetLatLongByAddress(pinDetail_frgmt.adressEditText.Text).Result;
                pinDetail_frgmt.unfinishedPin.name = pinDetail_frgmt.pinNameET.Text;
                pinDetail_frgmt.unfinishedPin.text = pinDetail_frgmt.pinTextET.Text;
                if (location.Latitude != 0.0d)
                {
                    pinDetail_frgmt.unfinishedPin.lat = location.Latitude;
                }
                if (location.Longitude != 0.0d)
                {
                    pinDetail_frgmt.unfinishedPin.lon = location.Longitude;
                }
                pinDetail_frgmt.mainActivity.firebaseDB.SavePin(pinDetail_frgmt.unfinishedPin);
            }
            ((MainActivity)pinDetail_frgmt.Activity).ReplaceContent(sender, e, new List<Android.Support.V4.App.Fragment>() { new Map_frgmt() }, "map");
        }
    }

    public class PinAdapter : RecyclerView.Adapter
    {
        List<Pin> pinList;
        MainActivity main;
        public PinAdapter(List<Pin> pins, MainActivity pMain)
        {
            if (pinList == null)
            {
                pinList = new List<Pin>();
            }

            pinList = GetMyPins(pins);
            main = pMain;
            main.firebaseDB.pinUpdate += FirebaseDB_pinUpdate;
        }

        internal List<Pin> GetMyPins(List<Pin> allPins)
        {
            List<Pin> myPins = new List<Pin>();
            foreach (Pin pin in allPins)
            {
                if (pin.members != null && pin.members.Exists(x => Convert.ToString(x.memberUid) == AuthWithFirebase.auth.Uid))
                {
                    if (!myPins.Contains(pin))
                    {
                        myPins.Add(pin);
                    }
                }
            }
            return myPins;
        }
        private void FirebaseDB_pinUpdate(object sender, EventArgs e)
        {
            pinList = GetMyPins(main.firebaseDB.appDB.pin);
            this.NotifyDataSetChanged();
        }

        public override int ItemCount => pinList.Count;
        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }
        public event EventHandler<int> ItemClick;
        public override int GetItemViewType(int position)
        {
            if (pinList[position].members.Exists(x => x.memberUid == AuthWithFirebase.auth.Uid && x.isCreator == true))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            PinItemViewHolder vh = holder as PinItemViewHolder;
            vh.PinName.Text = pinList[position].name;
            vh.PinLatLong.Text = "Lat: " + pinList[position].lat + " Lon: " + pinList[position].lon;
            vh.PinAdress.Text = MapHelpers.LocationHelpers.GetAdressForLatLong(new LocationObject(pinList[position].lat, pinList[position].lon, 0)).Result;
            vh.PinDescription.Text = pinList[position].text;
            if (vh.ItemViewType == 0)
            {
                vh.UserIcon.Text = main.firebaseDB.FindUserByUID(pinList[position].members.Find(x => x.isCreator == true).memberUid).icon;
            }
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            View itemView = new View(parent.Context);
            switch (viewType)
            {
                case 0:
                    itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.pinListItem, parent, false);
                    break;
                case 1:
                    itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.pinListItem_own, parent, false);
                    break;
                default:
                    break;
            }

            PinItemViewHolder vh = new PinItemViewHolder(itemView, OnClick);
            return vh;
        }
    }
    public class PinItemViewHolder : RecyclerView.ViewHolder
    {
        public TextView PinName { get; private set; }
        public TextView PinAdress { get; private set; }
        public TextView PinLatLong { get; private set; }
        public TextView PinDescription { get; private set; }
        public TextView UserIcon { get; private set; }
        public ImageButton SharePinButton { get; private set; }

        public PinItemViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            // Locate and cache view references:
            PinName = itemView.FindViewById<TextView>(Resource.Id.pinNameTextView);
            PinAdress = itemView.FindViewById<TextView>(Resource.Id.pinAdressTextView);
            PinLatLong = itemView.FindViewById<TextView>(Resource.Id.pinLatLonTextView);
            PinDescription = itemView.FindViewById<TextView>(Resource.Id.pinDescTextView);
            UserIcon = itemView.FindViewById<TextView>(Resource.Id.friendIconTextView);
            SharePinButton = itemView.FindViewById<ImageButton>(Resource.Id.sharePinButton);
            itemView.Click += (sender, e) => listener(base.LayoutPosition);
        }
    }

    public class UserAdapter : RecyclerView.Adapter
    {
        List<DatabaseUser> userList;
        MainActivity main;
        FirebaseDB firebaseDB;
        Pin pin;
        public UserAdapter(MainActivity pMain, Pin pPin)
        {
            if (userList == null)
            {
                userList = new List<DatabaseUser>();
            }

            pin = pPin;
            main = pMain;
            firebaseDB = main.firebaseDB;
            main.firebaseDB.userUpdate += FirebaseDB_userUpdate;
            userList = GetMyUsers();
        }
        internal List<DatabaseUser> GetMyUsers()
        {
            return firebaseDB.PrepareFriendsList();
        }
        private void FirebaseDB_userUpdate(object sender, EventArgs e)
        {
            userList = GetMyUsers();
            this.NotifyDataSetChanged();
        }

        public override int ItemCount => userList.Count;
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            UserItemViewHolder vh = holder as UserItemViewHolder;
            vh.UserName.Text = userList[position].name;
            vh.UserIcon.Text = userList[position].icon;
            bool isSharedWithUser = pin.members.Exists(x => x.memberUid == userList[position].uid);
            vh.PinIsSharedWithUserButton.Checked = isSharedWithUser;

        }
        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }
        public event EventHandler<int> ItemClick;
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = new View(parent.Context);
            itemView = LayoutInflater.From(parent.Context).
                           Inflate(Resource.Layout.userListItem, parent, false);
            UserItemViewHolder vh = new UserItemViewHolder(itemView, OnClick);
            return vh;
        }
    }
    public class UserItemViewHolder : RecyclerView.ViewHolder
    {
        public TextView UserName { get; private set; }
        public TextView UserIcon { get; private set; }
        public ToggleButton PinIsSharedWithUserButton { get; private set; }
        public UserItemViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            // Locate and cache view references:
            UserName = itemView.FindViewById<TextView>(Resource.Id.pinUsernameTextView);
            UserIcon = itemView.FindViewById<TextView>(Resource.Id.pinUserIconTextView);
            PinIsSharedWithUserButton = itemView.FindViewById<ToggleButton>(Resource.Id.pinIsSharedWithUserToggleButton);
            PinIsSharedWithUserButton.Click += (sender, e) => listener(base.LayoutPosition);
        }
    }
}
