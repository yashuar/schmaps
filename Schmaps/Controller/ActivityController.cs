﻿using Android.Content;
using Android.Widget;
using Schmaps.Firebase;
using Schmaps.Models;
using System;
using System.Collections.Generic;
using Xamarin.Essentials;

namespace Schmaps.Controller
{
    class ActivityController
    {
        internal static void HandleNewIntent(ref Intent myIntent, FirebaseDB firebaseDB)
        {

            if (myIntent.GetBooleanExtra("registerUser", false))
            {
                DatabaseUser user = new DatabaseUser();
                user.lastSignedIn = myIntent.GetIntExtra("lastSignedIn", 0);
                user.icon = myIntent.GetStringExtra("icon");
                user.name = myIntent.GetStringExtra("name");
                user.publicKey = Schmaps.Crypto.CryptoHelper.CreateKeys();
                user.uid = AuthWithFirebase.auth.Uid;


                firebaseDB.RegisterUser(user);
                myIntent = null;
            }
            else if (myIntent.GetBooleanExtra("signOut", false))
            {
                firebaseDB.SignOut();
                SecureStorage.Remove("MyPrivateKey");
                myIntent = null;
            }
            else if (myIntent.GetBooleanExtra("changeUser", false))
            {
                DatabaseUser user = new DatabaseUser();
                user = firebaseDB.appDB.AppUser;
                string icon = myIntent.GetStringExtra("icon");
                string name = myIntent.GetStringExtra("name");
                if (!string.IsNullOrEmpty(icon))
                {
                    user.icon = icon;
                }
                if (!string.IsNullOrEmpty(name))
                {
                    user.name = name;
                }
                user.lastSignedIn = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                firebaseDB.SaveUser(user);
                myIntent = null;
            }
        }

        internal static void HandleAddNewFriend(MainActivity mainActivity, EditText uIdOfFriend)
        {
            List<string> allDBUsers = new List<string>();
            foreach (var item in mainActivity.firebaseDB.appDB.databaseUsers)
            {
                allDBUsers.Add(item.uid);
            }
            if (!allDBUsers.Contains(uIdOfFriend.Text))
            {
                uIdOfFriend.SetError("Diese ID ist uns nicht bekannt.", Android.Support.V4.Content.ContextCompat.GetDrawable(mainActivity.BaseContext, Resource.Drawable.baseline_error_24));
                return;
            }
            if (uIdOfFriend.Text == AuthWithFirebase.auth.Uid)
            {
                uIdOfFriend.SetError("Du kannst dich leider nicht selbst als Freund hinzufügen.", Android.Support.V4.Content.ContextCompat.GetDrawable(mainActivity.BaseContext, Resource.Drawable.baseline_error_24));
                return;
            }

            string friendName = "";
            try
            {
                friendName = mainActivity.firebaseDB.appDB.databaseUsers.Find(x => x.uid == uIdOfFriend.Text).name;
            }
            catch (Exception)
            {
                throw;
            }
            string userName = mainActivity.firebaseDB.appDB.AppUser.name;

            mainActivity.firebaseDB.StartConversation(uIdOfFriend.Text);
        }

        internal static void HandleConnectivity(MainActivity main)
        {
            var current = Connectivity.NetworkAccess;

            if (current != NetworkAccess.Internet)
            {
                Toast.MakeText(main, "Schmaps funktioniert leider nicht ohne Internet 😔", ToastLength.Short).Show();
            }
        }

        
    }
}