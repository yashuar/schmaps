﻿using Android.App;
using Android.Content;
using Firebase.Database;
using Schmaps.Models;
using Schmaps.Resources.fragments;
using Schmaps.Resources.fragments.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Schmaps.Firebase;

namespace Schmaps.Controller
{
    internal class MessageController
    {
        private FirebaseDB firebaseDB;

        public MessageController(FirebaseDB firebaseDB)
        {
            this.firebaseDB = firebaseDB;
        }

        internal void AddMessage(DataSnapshot snapshot, string previousChildName)
        {
            if (firebaseDB.appDB.message.Exists(x => x.firebaseID == snapshot.Key))
            {
                return;
            }
            Models.Message toAdd = DeserializeMessage(snapshot, previousChildName);
            if (!firebaseDB.appDB.message.Exists(x => x.firebaseID == toAdd.firebaseID))
            {
                firebaseDB.appDB.message.Add(toAdd);
                firebaseDB.OnMessageUpdate(new MessageEventArgs(toAdd.conversationId, toAdd.firebaseID));
            }
        }
        internal void RemoveMessage(DataSnapshot snapshot)
        {
            firebaseDB.appDB.message.Remove(firebaseDB.FindMessageByFirebaseID(snapshot.Key));
            firebaseDB.OnMessageUpdate(new MessageEventArgs(null, null));
        }

        private Models.Message DeserializeMessage(DataSnapshot snapshot, string previousChildName)
        {
            IEnumerable<DataSnapshot> msgIterable = snapshot.Children.ToEnumerable<DataSnapshot>();
            Models.Message toAdd = new Models.Message();

            toAdd.firebaseID = Convert.ToString(snapshot.Key);

            foreach (var item in msgIterable)
            {
                if (item.Key == "conversationId")
                {
                    toAdd.conversationId = Convert.ToString(item.Value);
                }
                else if (item.Key == "messageText")
                {
                    toAdd.messageText = Convert.ToString(item.Value);
                }
                else if (item.Key == "sender")
                {
                    toAdd.sender = Convert.ToString(item.Value);
                }
                else if (item.Key == "timestamp")
                {
                    toAdd.timestamp = Convert.ToInt32(item.Value);
                }
            }
            return toAdd;
        }
    }

    public class MessageEventArgs : EventArgs
    {
        public string ConvoID { get; set; }
        public string MsgID { get; set; }
        public MessageEventArgs(string convo, string message)
        {
            this.ConvoID = convo;
            this.MsgID = message;
        }
    }
}