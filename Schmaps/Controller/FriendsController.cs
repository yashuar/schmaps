﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Schmaps.Resources.fragments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schmaps.Controller
{
    class FriendsController
    {
        internal static void HandleConversationUpdate(MainActivity mainActivity, Friends_frgmt friends_frgmt)
        {
            friends_frgmt.friendsList = mainActivity.firebaseDB.PrepareFriendsList();
        }

        internal static void HandleUserUpdate(MainActivity mainActivity, Friends_frgmt friends_frgmt)
        {
            friends_frgmt.friendsList = mainActivity.firebaseDB.PrepareFriendsList();
        }
    }
}