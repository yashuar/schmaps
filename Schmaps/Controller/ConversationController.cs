﻿using Firebase.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using Schmaps.Models;
using Schmaps.Resources.fragments.Helpers;
using Schmaps.Firebase;

namespace Schmaps.Controller
{
    internal class ConversationController
    {
        FirebaseDB firebaseDB;
        public ConversationController(FirebaseDB db)
        {
            firebaseDB = db;
        }
        internal void AddConversation(DataSnapshot snapshot, string previousChildName)
        {
            Conversation conversation = DeserializeConversation(snapshot, previousChildName);
            if (firebaseDB.appDB.Conversations == null)
            {
                firebaseDB.appDB.Conversations = new List<Conversation>();
            }
            if (conversation.members.Exists(x => x == AuthWithFirebase.auth.Uid))
            {
                firebaseDB.appDB.Conversations.Add(conversation);
                firebaseDB.OnConversationUpdate(EventArgs.Empty);
            }
        }

        internal void ChangeConversation(DataSnapshot snapshot, string previousChildName)
        {
            firebaseDB.FindConversationByFirebaseID(snapshot.Key);
            firebaseDB.OnConversationUpdate(EventArgs.Empty);
        }

        internal void RemoveConversation(DataSnapshot snapshot)
        {
            firebaseDB.appDB.Conversations.Remove(firebaseDB.FindConversationByFirebaseID(snapshot.Key));
            firebaseDB.OnConversationUpdate(EventArgs.Empty);
        }

        private Conversation DeserializeConversation(DataSnapshot snapshot, string previousChildName)
        {
            IEnumerable<DataSnapshot> convoIterable = snapshot.Children.ToEnumerable<DataSnapshot>();
            Conversation conversation = new Conversation();

            conversation.firebaseID = Convert.ToString(snapshot.Key);

            conversation.members = new List<string>();
            foreach (var convo in convoIterable)
            {
                if (convo.Key == "conversationId")
                {
                    conversation.conversationId = Convert.ToString(convo.Value);
                }
                else if (convo.Key == "lastMessage")
                {
                    conversation.lastMessage = Convert.ToString(convo.Value);
                }
                else if (convo.Key == "members")
                {
                    IEnumerable<DataSnapshot> membersIterable = convo.Children.ToEnumerable<DataSnapshot>();
                    foreach (var member in membersIterable)
                    {
                        try
                        {
                            conversation.members.Add(Convert.ToString(member.Value));                            
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }
                }
            }
            return conversation;
        }
    }
}