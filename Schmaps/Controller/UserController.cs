﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Database;
using Schmaps.Models;
using Schmaps.Firebase;

namespace Schmaps.Controller
{
    public class UserController
    {
        private FirebaseDB firebaseDB;

        public UserController(FirebaseDB firebaseDB)
        {
            this.firebaseDB = firebaseDB;
        }

        internal void AddUser(DataSnapshot snapshot, string previousChildName)
        {
            DatabaseUser user = DeserializeUser(snapshot, previousChildName);

            if (user.uid == AuthWithFirebase.auth.Uid)
            {
                firebaseDB.appDB.AppUser = user;
            }

            if (!string.IsNullOrEmpty(user.uid) && !string.IsNullOrEmpty(user.name) && !string.IsNullOrEmpty(user.icon))
            {
                if (!firebaseDB.appDB.databaseUsers.Exists(x => x.uid == user.firebaseID))
                {
                    firebaseDB.appDB.databaseUsers.Add(user);
                    firebaseDB.OnUserUpdate(EventArgs.Empty);
                }

                if (firebaseDB.appDB.databaseUsers.Count == firebaseDB.appDB.userCount)
                {
                    firebaseDB.OnConversationUpdate(EventArgs.Empty);
                }
            }
        }

        internal void ChangeUser(DataSnapshot snapshot, string previousChildName)
        {

            firebaseDB.appDB.databaseUsers.Remove(firebaseDB.FindUserByFirebaseID(snapshot.Key));
            firebaseDB.appDB.databaseUsers.Add(DeserializeUser(snapshot, previousChildName));
            firebaseDB.OnUserUpdate(EventArgs.Empty);
        }

        internal void RemoveUser(DataSnapshot snapshot)
        {
            firebaseDB.appDB.databaseUsers.Remove(firebaseDB.FindUserByFirebaseID(snapshot.Key));
            firebaseDB.OnUserUpdate(EventArgs.Empty);
        }

        private DatabaseUser DeserializeUser(DataSnapshot snapshot, string previousChildName)
        {
            IEnumerable<DataSnapshot> userNodeIterable = snapshot.Children.ToEnumerable<DataSnapshot>();
            DatabaseUser user = new DatabaseUser();

            user.firebaseID = Convert.ToString(snapshot.Key);

            user.pendingConversationRequests = new List<string>();

            foreach (var item in userNodeIterable)
            {
                if (item.Key == "icon")
                {
                    user.icon = Convert.ToString(item.Value);
                }
                else if (item.Key == "lastSignedIn")
                {
                    user.lastSignedIn = Convert.ToInt32(item.Value);
                }
                else if (item.Key == "name")
                {
                    user.name = Convert.ToString(item.Value);
                }
                else if (item.Key == "uid")
                {
                    user.uid = Convert.ToString(item.Value);
                }
                else if (item.Key == "pubKey")
                {
                    user.publicKey = Convert.ToString(item.Value);
                }
                else if (item.Key == "pendingRequests")
                {

                    IEnumerable<DataSnapshot> requestsIterable = item.Children.ToEnumerable<DataSnapshot>();
                    foreach (var request in requestsIterable)
                    {
                        if (request.Key == "uid")
                        {
                            user.pendingConversationRequests.Add(Convert.ToString(request.Value));
                        }
                    }
                }

            }
            return user;
        }

    }
}