﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.Gms.Location;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Mapsui.Geometries;
using Mapsui.Projection;
using Newtonsoft.Json;
using Schmaps.Models;
using static Schmaps.Resources.fragments.Map_frgmt;

namespace Schmaps.MapHelpers
{
    class LocationHelpers
    {
        static HttpClient client;
        static string mapquestapiAdress = "https://www.mapquestapi.com/geocoding/v1/";
        public static async Task<string> GetAdressForLatLong(LocationObject location)
        {
            client = new HttpClient();
            // build Api-Request
            Uri uri = new Uri(string.Format(mapquestapiAdress + "reverse" + "?key=AJeP6RBGPrAlHMcAtOLvl9fetlVNAbfi" + "&location=" + location.Latitude.ToString(CultureInfo.CreateSpecificCulture("en-US")) + "," + location.Longitude.ToString(CultureInfo.CreateSpecificCulture("en-US")) + "&includeRoadMetadata=true&includeNearestIntersection=true", string.Empty));
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            // get response and deserialize into "Root"-object 
            HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false);
            Root responseAsObj = JsonConvert.DeserializeObject<Root>(response.Content.ReadAsStringAsync().Result);
            // build Adress-String from "Root"-object
            string adressFromResponse = responseAsObj.results[0].locations[0].adminArea1 + ", " + responseAsObj.results[0].locations[0].adminArea5 + ", " + responseAsObj.results[0].locations[0].postalCode + ", " + responseAsObj.results[0].locations[0].street;
            return adressFromResponse;
        }

        public static async Task<LocationObject> GetLatLongByAddress(string address)
        {

            client = new HttpClient();
            Uri uri = new Uri(string.Format(mapquestapiAdress + "address" + "?key=AJeP6RBGPrAlHMcAtOLvl9fetlVNAbfi" + "&location=" + address, string.Empty));
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            // get response and deserialize into "Root"-object 
            HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false);
            Root responseAsObj = JsonConvert.DeserializeObject<Root>(response.Content.ReadAsStringAsync().Result);
            LocationObject location = new LocationObject(responseAsObj.results[0].locations[0].latLng.lat, responseAsObj.results[0].locations[0].latLng.lng,0);
            return location;
        }
        public static async Task<Root> GetListByAddress(string address)
        {

            client = new HttpClient();
            Uri uri = new Uri(string.Format(mapquestapiAdress + "address" + "?key=AJeP6RBGPrAlHMcAtOLvl9fetlVNAbfi" + "&location=" + address, string.Empty));
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            // get response and deserialize into "Root"-object 
            HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false);
            Root responseAsObj = JsonConvert.DeserializeObject<Root>(response.Content.ReadAsStringAsync().Result);
            return responseAsObj;
        }

        public static BoundingBox FindBoundingBoxForListOfPins(List<Pin> pins)
        {
            List<Mapsui.Geometries.Point> points = new List<Mapsui.Geometries.Point>();
            foreach (var item in pins)
            {
                var point = SphericalMercator.FromLonLat(item.lon, item.lat);
                points.Add(point);
            }
            var x_query = from Point p in points select p.X;
            double xmin = x_query.Min();
            double xmax = x_query.Max();

            var y_query = from Point p in points select p.Y;
            double ymin = y_query.Min();
            double ymax = y_query.Max();
            BoundingBox box = new BoundingBox(xmin, ymin, xmax, ymax);
            return box;
        }
    }


    #region classes for deserialization
    public class Copyright
    {
        public string text { get; set; }
        public string imageUrl { get; set; }
        public string imageAltText { get; set; }
    }

    public class Info
    {
        public int statuscode { get; set; }
        public Copyright copyright { get; set; }
        public List<object> messages { get; set; }
    }

    public class Options
    {
        public int maxResults { get; set; }
        public bool thumbMaps { get; set; }
        public bool ignoreLatLngInput { get; set; }
    }

    public class LatLng
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class ProvidedLocation
    {
        public LatLng latLng { get; set; }
    }

    public class LatLng2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class DisplayLatLng
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Location
    {
        public string street { get; set; }
        public string adminArea6 { get; set; }
        public string adminArea6Type { get; set; }
        public string adminArea5 { get; set; }
        public string adminArea5Type { get; set; }
        public string adminArea4 { get; set; }
        public string adminArea4Type { get; set; }
        public string adminArea3 { get; set; }
        public string adminArea3Type { get; set; }
        public string adminArea1 { get; set; }
        public string adminArea1Type { get; set; }
        public string postalCode { get; set; }
        public string geocodeQualityCode { get; set; }
        public string geocodeQuality { get; set; }
        public bool dragPoint { get; set; }
        public string sideOfStreet { get; set; }
        public string linkId { get; set; }
        public string unknownInput { get; set; }
        public string type { get; set; }
        public LatLng2 latLng { get; set; }
        public DisplayLatLng displayLatLng { get; set; }
        public string mapUrl { get; set; }
        public object nearestIntersection { get; set; }
        public object roadMetadata { get; set; }
    }

    public class Result
    {
        public ProvidedLocation providedLocation { get; set; }
        public List<Location> locations { get; set; }
    }

    public class Root
    {
        public Info info { get; set; }
        public Options options { get; set; }
        public List<Result> results { get; set; }
    }
    #endregion
}