﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Firebase.Annotations;
using Firebase.Database;
using Schmaps.Firebase;
using Xamarin.Essentials;

namespace Schmaps.Models
{

    public class DatabaseUser
    {
        public string firebaseID;

        public string icon;
        public int lastSignedIn;
        public string name;

        public string publicKey;

        // this is a List of uids of users wanting to start a Conversations with the DatabaseUser, for a future implementation
        public List<string> pendingConversationRequests;
        public string uid;
    }
}