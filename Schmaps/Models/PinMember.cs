﻿namespace Schmaps.Models
{
    public class PinMember
    {
        public bool isCreator;
        public string memberUid;

        public PinMember()
        {
        }

        public PinMember(bool v, string uid)
        {
            isCreator = v;
            memberUid = uid;
        }
    }
}