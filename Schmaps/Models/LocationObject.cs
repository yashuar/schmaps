﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schmaps.Models
{
    public class LocationObject
    {
        public double Latitude;

        public double Longitude;

        public double? Altitude;
        public LocationObject(double cLat, double cLng, double? cAlt)
        {
            Latitude = cLat;
            Longitude = cLng;
            Altitude = cAlt;
        }
    }
}