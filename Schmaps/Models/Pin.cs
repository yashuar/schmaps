﻿using System.Collections.Generic;

namespace Schmaps.Models
{
    public class Pin
    {
        public string firebaseID;

        public string name;
        public double lat;
        public double lon;
        public string text;
        public List<PinMember> members;
        public bool isUnfinished;
        public Pin()
        { }
        public Pin(string pName, double pLat, double pLon, string pText, PinMember pinMember, bool pIsUnfinished)
        {
            name = pName;
            lat = pLat;
            lon = pLon;
            text = pText;
            members = new List<PinMember>();
            members.Add(pinMember);
            isUnfinished = pIsUnfinished;
        }
    }
}