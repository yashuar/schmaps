﻿using System.Collections.Generic;

namespace Schmaps.Models
{
    public class Conversation
    {
        public string firebaseID;

        public List<string> members;
        public string conversationId;
        public string lastMessage;
        public Conversation()
        {

        }
        public Conversation(List<string> pMembers, string pConversationId, string pLastMessage)
        {
            members = pMembers;
            conversationId = pConversationId;
            lastMessage = pLastMessage;
        }
    }
}