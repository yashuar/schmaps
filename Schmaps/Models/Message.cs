﻿using System;

namespace Schmaps.Models
{
    public class Message
    {
        public string firebaseID;

        public string sender;
        public string conversationId;
        public string messageText;
        public int timestamp;
        public string humanReadableTime
        {
            get
            {
                int unixTimestampNow = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                int unixTimestampMidnight = (int)(DateTime.Today.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                if (unixTimestampNow - unixTimestampMidnight > unixTimestampNow - timestamp)
                {
                    // convert UnixTime to HumandReadableTime, done here as this is before showing the Chat, but after reading it from Firebase
                    return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(timestamp).ToLocalTime().ToShortTimeString();
                }
                else
                {
                    // convert UnixTime to HumandReadableTime, done here as this is before showing the Chat, but after reading it from Firebase
                    return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(timestamp).ToLocalTime().ToShortDateString();

                }
            }
        }

        public Message()
        { }
        public Message(string convoId, string pSender, string pMsgText, int pTimeStamp)
        {
            conversationId = convoId;
            sender = pSender;
            messageText = pMsgText;
            timestamp = pTimeStamp;
        }
    }
}