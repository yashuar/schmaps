﻿using System.Collections.Generic;
using Android.Runtime;

namespace Schmaps.Models
{
    public class AppDB : Java.Lang.Object, IJavaObject
    {
        public MainActivity main;

        private Dictionary<string, List<Message>> chats;
        public Dictionary<string, List<Message>> Chats
        {
            get
            {
                return chats;
            }
            set
            {
                chats = value;
            }
        }
        public List<string> ListOfFriends
        {
            get;
            set;
        }

        private List<Conversation> conversation;
        public List<Conversation> Conversations
        {
            get
            {
                return conversation;
            }
            set
            {
                conversation = value;
                //TODO ADD CONVERSATION ADDED EVENT
            }
        }
        public List<Message> message;
        public List<Pin> pin;
        public DatabaseUser appUser;
        public DatabaseUser AppUser
        {
            get { return appUser; }
            set { appUser = value; }
        }
        public List<DatabaseUser> databaseUsers;

        public int userCount { get; set; }
    }
}