﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Support.Design.Widget;
using Android.Views;
using Schmaps.Resources.fragments;
using System.Collections.Generic;
using Plugin.CurrentActivity;
using System;
using Android.Graphics.Drawables;
using Java.Lang;
using Schmaps.Models;
using Schmaps.Firebase;
using Java.Util;
using Firebase.Database;
using System.Runtime.CompilerServices;
using Android.Content;
using Schmaps.Resources.fragments.Helpers;
using Com.Wang.Avi;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Android.Content.PM;
using Schmaps.Controller;

namespace Schmaps
{
    [Activity(Label = "@string/app_name", Theme = "@style/MyTheme", MainLauncher = true, WindowSoftInputMode = SoftInput.AdjustResize, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity, BottomNavigationView.IOnNavigationItemSelectedListener
    {
        List<Android.Support.V4.App.Fragment> fragments;
        public static Android.Support.V4.App.FragmentManager MyFragmentManager;
        public Android.Support.V7.Widget.Toolbar toolbar;
        public string toolbarTitle;
        public AVLoadingIndicatorView loadingIndicator;
        public FirebaseDB firebaseDB;
        Intent myIntent;
        Intent oldIntent;

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            if (myIntent == null && oldIntent == null)
            {
                myIntent = intent;
                oldIntent = intent;
            }
            else if (intent != oldIntent && oldIntent != null)
            {
                myIntent = intent;
                oldIntent = intent;
            }
        }
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            // prepare view(s)
            SetContentView(Resource.Layout.activity_main);


            loadingIndicator = FindViewById<AVLoadingIndicatorView>(Resource.Id.loadingIndicator);
            loadingIndicator.SetIndicator("LineSpinFadeLoaderIndicator");
            loadingIndicator.Show();

            toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            base.SetSupportActionBar(toolbar);
            toolbarTitle = "Schmaps";
            SupportActionBar.Title = toolbarTitle;
            ActionbarConfig("");

            BottomNavigationView navigation = FindViewById<BottomNavigationView>(Resource.Id.navigation);
            navigation.SetOnNavigationItemSelectedListener(this);

            fragments = new List<Android.Support.V4.App.Fragment>();
            fragments.Add(new Friends_frgmt());
            fragments.Add(new Map_frgmt());
            MyFragmentManager = SupportFragmentManager;

            if (firebaseDB == null)
            {
                firebaseDB = new FirebaseDB(this);
            }
            firebaseDB.InitFinished += OnFirebaseInitFinished;
            firebaseDB.userUpdate += FirebaseDB_userUpdate;

            AuthWithFirebase.SignInAnon();
            firebaseDB.InitiateAndSetListeners();

            //show friendlist
            SupportFragmentManager.BeginTransaction().
                            Replace(Resource.Id.content_layout, fragments[0])
                        .Commit();
            //Toast.MakeText(Application.Context, "uid : " + AuthWithFirebase.auth.Uid, ToastLength.Short).Show();
        }
        private void FirebaseDB_userUpdate(object sender, EventArgs e)
        {
            if (firebaseDB.appDB.databaseUsers.Count == firebaseDB.appDB.userCount)
            {
                loadingIndicator.SmoothToHide();
                if (!firebaseDB.AppUserIsRegistered())
                {
                    StartActivity(typeof(RegistrationActivity));
                }
            }

        }

        private void OnFirebaseInitFinished(object sender, EventArgs e)
        {
            //
        }

        protected override void OnResume()
        {
            base.OnResume();

           
            ActivityController.HandleConnectivity(this);
            
            if (myIntent != null)
            {
                ActivityController.HandleNewIntent(ref myIntent, firebaseDB);
            }
        }


        public override void OnBackPressed()
        {
            int count = MyFragmentManager.BackStackEntryCount;
            if (count == 0)
            {
                base.OnBackPressed();
            }
            else
            {
                SupportActionBar.SetDisplayHomeAsUpEnabled(false);
                MyFragmentManager.PopBackStack();
                MySupportFragment frag = (MySupportFragment)MyFragmentManager.FindFragmentByTag("chat");
                if (frag != null && frag.IsVisible)
                {
                    toolbarTitle = "Schmaps";
                    SupportActionBar.Title = toolbarTitle;
                }
            }
        }
        public void ActionbarConfig(string toggle)
        {
            SupportActionBar.SetHomeButtonEnabled(true);
            if (!System.String.IsNullOrEmpty(toggle))
            {
                if (toggle == "back on")
                {
                    SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                }
                else if (toggle == "back off")
                {
                    SupportActionBar.SetDisplayHomeAsUpEnabled(false);
                }
                else
                {
                }
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    if (MyFragmentManager.BackStackEntryCount != 0)
                    {
                        MyFragmentManager.PopBackStack();
                        SupportActionBar.SetDisplayHomeAsUpEnabled(false);
                        MySupportFragment frag = (MySupportFragment)MyFragmentManager.FindFragmentByTag("chat");
                        if (frag != null && frag.IsVisible)
                        {
                            toolbarTitle = "Schmaps";
                            SupportActionBar.Title = toolbarTitle;
                        }

                    }
                    return true;
                case Resource.Id.action_add_friend:
                    //Toast.MakeText(this, "Action Add Friend selected", ToastLength.Short).Show();
                    StartAddFriendAction();
                    return true;
                case Resource.Id.action_open_pin_list:
                    //Toast.MakeText(this, "Action Open Pin List selected", ToastLength.Short).Show();
                    StartOpenPinListAction();
                    return true;
                case Resource.Id.action_open_settings:
                    //Toast.MakeText(this, "Action Open Settings selected", ToastLength.Short).Show();
                    StartOpenSettingsAction();
                    return true;
                default:
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }
        public void StartOpenSettingsAction()
        {
            Intent settings = new Intent(this, typeof(SettingsActivity));
            settings.PutExtra("icon", firebaseDB.appDB.AppUser.icon);
            settings.PutExtra("name", firebaseDB.appDB.AppUser.name);
            StartActivity(settings);
        }

        public void StartOpenPinListAction()
        {
            MyFragmentManager.BeginTransaction().
                             Replace(Resource.Id.content_layout, new PinList_frgmt())
                             .AddToBackStack(null)
                         .Commit();
        }

        public void StartAddFriendAction()
        {
            MyFragmentManager.BeginTransaction().
                             Replace(Resource.Id.content_layout, new AddFriend_frgmt())
                             .AddToBackStack(null)
                         .Commit();
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.navigation_friends:
                    SupportFragmentManager.BeginTransaction().
                        Replace(Resource.Id.content_layout, fragments[0])
                        .AddToBackStack(null)
                        .Commit();
                    SupportActionBar.SetDisplayHomeAsUpEnabled(false);
                    //Toast.MakeText(this, "Friends selected", ToastLength.Short).Show();
                    return true;

                case Resource.Id.navigation_map:
                    SupportFragmentManager.BeginTransaction().
                        Replace(Resource.Id.content_layout, fragments[1])
                        .AddToBackStack(null)
                        .Commit();
                    SupportActionBar.SetDisplayHomeAsUpEnabled(false);
                    //Toast.MakeText(this, "Maps selected", ToastLength.Short).Show();
                    return true;
                default:
                    break;
            }
            return false;
        }

        //this is done here because of the fragmentmangers scope
        internal void StartChat(object sender, AdapterView.ItemClickEventArgs e, Chat_frgmt chat, DatabaseUser user, string fragmnt_tag)
        {
            chat.Arguments.PutString("user", user.uid);
            MyFragmentManager.BeginTransaction()
                .SetCustomAnimations(Resource.Animation.abc_fade_in, Resource.Animation.abc_fade_out)
                .Replace(Resource.Id.content_layout, chat, fragmnt_tag)
                              .AddToBackStack(null)
                          .Commit();
        }
        internal void ReplaceContent(object sender, EventArgs e, List<Android.Support.V4.App.Fragment> fragments, string fragmnt_tag)
        {
            MyFragmentManager.BeginTransaction()
                                .SetCustomAnimations(Resource.Animation.abc_fade_in, Resource.Animation.abc_fade_out)
                               .Replace(Resource.Id.content_layout, fragments[0], fragmnt_tag)
                              .AddToBackStack(null)
                          .Commit();
        }
    }
}