﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Com.Wang.Avi;
using Schmaps.Firebase;
using Schmaps.Models;

namespace Schmaps
{
    [Activity(Label = "RegistrationActivity", WindowSoftInputMode = SoftInput.AdjustResize, ScreenOrientation = ScreenOrientation.Portrait)]
    public class RegistrationActivity : AppCompatActivity
    {
        EditText nickNameET;
        EditText iconET;
        Button registerButton;
        AVLoadingIndicatorView avi;
        TextView hintText;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_registration);
            nickNameET = FindViewById<EditText>(Resource.Id.nickNameEditText);
            iconET = FindViewById<EditText>(Resource.Id.iconEditText);
            registerButton = FindViewById<Button>(Resource.Id.registerButton);
            hintText = FindViewById<TextView>(Resource.Id.hintText);
            avi = FindViewById<AVLoadingIndicatorView>(Resource.Id.regLoadingIndicator);
            avi.SetIndicator("LineSpinFadeLoaderIndicator");
            avi.Hide();
            avi.Visibility = ViewStates.Gone;
            registerButton.Click += RegisterButton_Click;
            // Create your application here
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            avi.Visibility = ViewStates.Visible;
            avi.Show();
            nickNameET.Visibility = ViewStates.Gone;
            iconET.Visibility = ViewStates.Gone;
            hintText.Visibility = ViewStates.Gone;
            registerButton.Clickable = false;
            var main = new Intent(this, typeof(MainActivity));
            main.PutExtra("registerUser", true);
            main.PutExtra("lastSignedIn", (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
            main.PutExtra("name", nickNameET.Text);
            main.PutExtra("icon", iconET.Text);
            main.AddFlags(ActivityFlags.ReorderToFront);

            StartActivity(main);
            OverridePendingTransition(Resource.Animation.abc_fade_in, Resource.Animation.abc_fade_out);
        }
        public override void OnBackPressed()
        {

        }
    }
}