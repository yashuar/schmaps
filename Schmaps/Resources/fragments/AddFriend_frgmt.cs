﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Icu.Text;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Schmaps.Controller;
using Schmaps.Firebase;
using Schmaps.Resources.fragments.Helpers;

namespace Schmaps.Resources.fragments
{
    public class AddFriend_frgmt : MySupportFragment
    {
        Button addFriendButton;
        EditText uIdOfFriend;
        TextView uIdOfUser;
        View addFriendFragment;
        List<Android.Support.V4.App.Fragment> fragments;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            base.mainActivity = ((MainActivity)this.Activity);
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            addFriendFragment = inflater.Inflate(Resource.Layout.fragment_addFriend, container, false);
            addFriendButton = addFriendFragment.FindViewById<Button>(Resource.Id.addFriendButton);
            uIdOfFriend = addFriendFragment.FindViewById<EditText>(Resource.Id.friendToAddUID);
            uIdOfUser = addFriendFragment.FindViewById<TextView>(Resource.Id.userUid);
            uIdOfUser.Text = AuthWithFirebase.auth.Uid;
            addFriendButton.Click += AddFriendButton_Click;

            return addFriendFragment;
        }

        private void AddFriendButton_Click(object sender, EventArgs e)
        {

            fragments = new List<Android.Support.V4.App.Fragment>();
            fragments.Add(new Friends_frgmt());

            ActivityController.HandleAddNewFriend(mainActivity, uIdOfFriend);
            
            ((MainActivity)this.Activity).ReplaceContent(sender, e, fragments, "friends");
        }
    }
}