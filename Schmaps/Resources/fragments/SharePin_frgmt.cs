﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Schmaps.Controller;
using Schmaps.Firebase;
using Schmaps.Models;
using Schmaps.Resources.fragments.Helpers;

namespace Schmaps.Resources.fragments
{
    public class SharePin_frgmt : MySupportFragment
    {
        View sharePinView;
        RecyclerView sharePinRV;
        UserAdapter adapter;
        public Pin pin;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            mainActivity = (MainActivity)this.Activity;

            pin = PinController.GetPinForSharePin(this, Arguments);
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            sharePinView = inflater.Inflate(Resource.Layout.fragment_sharePin, container, false);
            sharePinRV = sharePinView.FindViewById<RecyclerView>(Resource.Id.sharePinRecyclerView);

            adapter = new UserAdapter(mainActivity, pin);
            adapter.ItemClick += Adapter_ItemClick;
            sharePinRV.SetAdapter(adapter);
            LinearLayoutManager manager = new LinearLayoutManager(sharePinView.Context);
            manager.StackFromEnd = false;
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(sharePinView.Context, manager.Orientation);
            dividerItemDecoration.SetDrawable(Android.Support.V4.Content.ContextCompat.GetDrawable(Context, Resource.Drawable.divider));
            sharePinRV.AddItemDecoration(dividerItemDecoration);
            RecyclerView.LayoutManager layoutManager = manager;
            sharePinRV.SetLayoutManager(layoutManager);

            return sharePinView;
        }

        private void Adapter_ItemClick(object sender, int e)
        {
            PinController.HandleSharePin_ListItemClicked(this, e);
        }
    }
}