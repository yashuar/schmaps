﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Schmaps.Controller;
using Schmaps.Firebase;
using Schmaps.MapHelpers;
using Schmaps.Models;
using Schmaps.Resources.fragments.Helpers;
using static Schmaps.Resources.fragments.Map_frgmt;

namespace Schmaps.Resources.fragments
{
    public class PinDetail_frgmt : MySupportFragment
    {
        private View pinDetailFragment;
        public EditText pinNameET;
        public EditText pinTextET;
        private Button addPinButton;
        private TextView latTextView;
        private TextView lonTextView;
        private TextView adressTextView;
        public TextView adressEditText;
        public Pin unfinishedPin;
        private string address;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            base.mainActivity = ((MainActivity)this.Activity);

            unfinishedPin = PinController.FindUnfinishedPin(this);

            address = PinController.PrepareAdressForPinDetail(unfinishedPin);
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            pinDetailFragment = inflater.Inflate(Resource.Layout.fragment_pinDetail, container, false);
            pinNameET = pinDetailFragment.FindViewById<EditText>(Resource.Id.pinNameET);
            pinTextET = pinDetailFragment.FindViewById<EditText>(Resource.Id.pinTextET);
            latTextView = pinDetailFragment.FindViewById<TextView>(Resource.Id.latTextView);
            lonTextView = pinDetailFragment.FindViewById<TextView>(Resource.Id.lonTextView);
            adressTextView = pinDetailFragment.FindViewById<TextView>(Resource.Id.adressTextView);
            adressEditText = pinDetailFragment.FindViewById<TextView>(Resource.Id.pinAddressET);
            addPinButton = pinDetailFragment.FindViewById<Button>(Resource.Id.addPinButton);
            addPinButton.Click += AddPinButton_Click;
            latTextView.Text = "Breite: "+ Convert.ToString(unfinishedPin.lat);
            lonTextView.Text = "Länge: " + Convert.ToString(unfinishedPin.lon);
            adressEditText.Text = address;
            if (unfinishedPin.name != null && unfinishedPin.name != String.Empty)
            {
                pinNameET.Text = unfinishedPin.name;
            }
            if (unfinishedPin.text != null && unfinishedPin.text != String.Empty)
            {
                pinTextET.Text = unfinishedPin.text;
            }
            return pinDetailFragment;
        }

        private void AddPinButton_Click(object sender, EventArgs e)
        {
            PinController.HandlePinDetailSaveButtonClick(this, sender, e);
        }
    }
}