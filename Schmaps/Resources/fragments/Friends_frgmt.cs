﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Wang.Avi;
using Schmaps.Controller;
using Schmaps.Firebase;
using Schmaps.Models;
using Schmaps.Resources.fragments.Helpers;
using Xamarin.Essentials;

namespace Schmaps.Resources.fragments
{
    public class Friends_frgmt : MySupportFragment
    {
        public View myFragment;
        public RecyclerView friendsView;
        private AVLoadingIndicatorView avi;
        public FriendsAdapter friendsAdapter;
        public List<DatabaseUser> friendsList;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            mainActivity = ((MainActivity)Activity);
            avi = mainActivity.loadingIndicator;
            mainActivity.firebaseDB.userUpdate += FirebaseDB_userUpdate;
            mainActivity.firebaseDB.conversationUpdate += FirebaseDB_conversationUpdate;
            friendsList = mainActivity.firebaseDB.PrepareFriendsList();
            // Create your fragment here
        }

        private void FirebaseDB_conversationUpdate(object sender, EventArgs e)
        {
            FriendsController.HandleConversationUpdate(mainActivity, this);
            ToggleEmptyView();
        }

        private void FirebaseDB_userUpdate(object sender, EventArgs e)
        {
            FriendsController.HandleUserUpdate(mainActivity, this);
            ToggleEmptyView();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            myFragment = inflater.Inflate(Resource.Layout.fragment_friends, container, false);
            friendsView = myFragment.FindViewById<RecyclerView>(Resource.Id.friendsRecyclerView);
            friendsAdapter = new FriendsAdapter(friendsList, mainActivity.firebaseDB);
            friendsAdapter.ItemClick += OnItemClick;
            friendsView.SetAdapter(friendsAdapter);
            LinearLayoutManager manager = new LinearLayoutManager(friendsView.Context);
            manager.StackFromEnd = false;
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(friendsView.Context, manager.Orientation);
            dividerItemDecoration.SetDrawable(Android.Support.V4.Content.ContextCompat.GetDrawable(Context, Resource.Drawable.divider));
            friendsView.AddItemDecoration(dividerItemDecoration);
            RecyclerView.LayoutManager layoutManager = manager;
            friendsView.SetLayoutManager(layoutManager);

            return myFragment;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            avi.Show();
        }
        public override void OnResume()
        {
            base.OnResume();
            ToggleEmptyView();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
        }
        void OnItemClick(object sender, int position)
        {
            Toast.MakeText(Application.Context, "Friend " + base.mainActivity.firebaseDB.FindUserByUID(base.mainActivity.firebaseDB.appDB.ListOfFriends[position]).name + " selected", ToastLength.Short).Show();
            Chat_frgmt chat = new Chat_frgmt();
            chat.Arguments = new Bundle();
            chat.Arguments.PutString("name", base.mainActivity.firebaseDB.FindUserByUID(base.mainActivity.firebaseDB.appDB.ListOfFriends[position]).name);
            ((MainActivity)this.Activity).StartChat(sender, null, chat, base.mainActivity.firebaseDB.FindUserByUID(base.mainActivity.firebaseDB.appDB.ListOfFriends[position]), "chat");
        }
        public void ToggleEmptyView()
        {
            var current = Connectivity.NetworkAccess;
            if (friendsList.Count > 0)
            {
                myFragment.FindViewById<TextView>(Resource.Id.EmptyView)
                        .Visibility = ViewStates.Gone;
                avi.SmoothToHide();
            }
            else if (current != NetworkAccess.Internet)
            {
                myFragment.FindViewById<TextView>(Resource.Id.EmptyView)
                           .Visibility = ViewStates.Gone;
            }
        }
    }
    public class FriendsAdapter : RecyclerView.Adapter
    {
        public List<DatabaseUser> friendsList;
        private FirebaseDB firebaseDB;

        public override int ItemCount => friendsList.Count;

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }
        private void OnUpdate(object sender, EventArgs e)
        {
            friendsList = firebaseDB.PrepareFriendsList();
            NotifyDataSetChanged();
        }

        public event EventHandler<int> ItemClick;

        public FriendsAdapter(List<DatabaseUser> pFriendsList, FirebaseDB db)
        {
            firebaseDB = db;
            firebaseDB.userUpdate += OnUpdate;
            firebaseDB.conversationUpdate += OnUpdate;
            friendsList = pFriendsList;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            FriendsViewHolder vh = holder as FriendsViewHolder;
            vh.UserName.Text = friendsList[position].name;
            vh.UserIcon.Text = friendsList[position].icon;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = new View(parent.Context);
            itemView = LayoutInflater.From(parent.Context).
                           Inflate(Resource.Layout.friendslistItem, parent, false);
            FriendsViewHolder vh = new FriendsViewHolder(itemView, OnClick);
            return vh;
        }
    }
    public class FriendsViewHolder : RecyclerView.ViewHolder
    {
        public TextView UserName { get; private set; }
        public TextView UserIcon { get; private set; }
        public FriendsViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            // Locate and cache view references:
            UserName = itemView.FindViewById<TextView>(Resource.Id.usernameTextView);
            UserIcon = itemView.FindViewById<TextView>(Resource.Id.userIconTextView);
            itemView.Click += (sender, e) => listener(base.LayoutPosition);
        }
    }
}