﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Schmaps.Firebase;

namespace Schmaps.Resources.fragments.Helpers
{
    public class MySupportFragment : Android.Support.V4.App.Fragment
    {
        public MainActivity mainActivity;
        public void SetToolbarTitle(string newTitle)
        {
            mainActivity.toolbarTitle = newTitle;
            mainActivity.SupportActionBar.Title = mainActivity.toolbarTitle;
        }
    }
}