﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Firebase.Database;
using Java.Util;
using Plugin.Geolocator.Abstractions;
using Schmaps.Controller;
using Schmaps.Firebase;
using Schmaps.Models;
using Schmaps.Resources.fragments.Helpers;
using Xamarin.Essentials;

namespace Schmaps.Resources.fragments
{
    public class Chat_frgmt : MySupportFragment
    {
        DatabaseUser AppUser;
        string ChatPartnerPubKey;
        View ChatFragment;
        FloatingActionButton ChatButton;
        EditText ChatText;
        Conversation conversation;
        public RecyclerView RChatView;
        public ChatAdapter chatAdapter;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            base.mainActivity = ((MainActivity)this.Activity);
            base.mainActivity.ActionbarConfig("back on");
            ChatPartnerPubKey = mainActivity.firebaseDB.FindUserByUID(Arguments.GetString("user")).publicKey;
            // Create your fragment here
        }

        public override void OnResume()
        {
            base.OnResume();
            base.mainActivity.ActionbarConfig("back on");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            ChatFragment = inflater.Inflate(Resource.Layout.fragment_chat, container, false);

            GetFirebaseUser();
            #region Set to Chat

            string newTitle = this.Arguments.GetString("name");
            this.SetToolbarTitle(newTitle);
            #endregion

            RChatView = ChatFragment.FindViewById<RecyclerView>(Resource.Id.list_of_messages);            
            chatAdapter = new ChatAdapter(mainActivity.firebaseDB, Arguments.GetString("user"), RChatView);
            RChatView.SetAdapter(chatAdapter); LinearLayoutManager manager = new LinearLayoutManager(ChatFragment.Context);
            manager.StackFromEnd = true;
            RecyclerView.LayoutManager layoutManager = manager;
            RChatView.SetLayoutManager(layoutManager);
            ChatButton = ChatFragment.FindViewById<FloatingActionButton>(Resource.Id.chatFab);
            ChatText = ChatFragment.FindViewById<EditText>(Resource.Id.chatEditText);
            ChatButton.Click += ChatButton_Click;
            return ChatFragment;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            base.mainActivity.ActionbarConfig("back off");
            string newTitle = "Schmaps";
            this.SetToolbarTitle(newTitle);
        }
        public override void OnPause()
        {
            base.OnPause();
            base.mainActivity.ActionbarConfig("back off");
            string newTitle = "Schmaps";
            this.SetToolbarTitle(newTitle);
        }

        private void GetFirebaseUser()
        {
            if (AppUser == null && mainActivity.firebaseDB.appDB.AppUser != null)
            {
                AppUser = mainActivity.firebaseDB.appDB.AppUser;
            }
            else
            {

            }
        }

        private void ChatButton_Click(object sender, EventArgs e)
        {
            ChatController.HandleChatButtonClick(mainActivity, ChatText, Arguments, ChatPartnerPubKey);
        }
    }
}