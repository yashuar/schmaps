﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Java.Lang;
using Mapsui;
using Mapsui.Geometries;
using Mapsui.Layers;
using Mapsui.Projection;
using Mapsui.Providers;
using Mapsui.Styles;
using Mapsui.UI;
using Mapsui.UI.Android;
using Mapsui.Utilities;
using Org.Apache.Http.Impl.Client;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Schmaps.Controller;
using Schmaps.Firebase;
using Schmaps.MapHelpers;
using Schmaps.Models;
using Schmaps.Resources.fragments.Helpers;
using Xamarin.Essentials;
using Map = Mapsui.Map;

namespace Schmaps.Resources.fragments
{
    public class Map_frgmt : MySupportFragment
    {
        private static LocationObject location = new LocationObject(52.520008, 13.404954, 1);
        public MapControl mapcontrol;
        View MapFragment;
        FloatingActionButton mapLocationFAB;
        FloatingActionButton mapAddPinFAB;
        public bool addPinIsActive;
        EditText searchbarEditText;
        ImageButton searchbarImageButton;
        
        public List<Pin> pinList
        {
            get
            {
                return MapController.GetPinList(this);
            }
            set
            {
                MapController.SetPinList(this, value);
            }
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            base.mainActivity = ((MainActivity)this.Activity);
            // Create your fragment here
            CrossGeolocator.Current.PositionChanged += Current_PositionChanged;

            MapController.OnCreate_HandleUnfinishedPins(this);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            StartLocationListening();
            MapFragment = inflater.Inflate(Resource.Layout.fragment_map, container, false);
            mapcontrol = MapFragment.FindViewById<MapControl>(Resource.Id.mapControl);
            mapLocationFAB = MapFragment.FindViewById<FloatingActionButton>(Resource.Id.mapFAB);
            mapLocationFAB.Click += MapLocationFAB_Click;
            mapAddPinFAB = MapFragment.FindViewById<FloatingActionButton>(Resource.Id.addFAB);
            mapAddPinFAB.Click += mapAddPinFAB_Click;
            searchbarEditText = MapFragment.FindViewById<EditText>(Resource.Id.searchbarSearchEditText);
            searchbarImageButton = MapFragment.FindViewById<ImageButton>(Resource.Id.searchbarRighthandImageButton);
            searchbarImageButton.Click += SearchbarImageButton_Click;
            searchbarEditText.EditorAction += SearchbarEditText_EditorAction;
            Setup(mapcontrol);
            if (addPinIsActive)
            {
                mapAddPinFAB.SetImageDrawable(ContextCompat.GetDrawable(this.Context, Resource.Drawable.outline_done_24));
            }
            return MapFragment;
        }

        private void SearchbarEditText_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            SearchbarImageButton_Click(sender, e);
        }

        private void SearchbarImageButton_Click(object sender, EventArgs e)
        {
            mapAddPinFAB_Click(sender, e);
            MapController.HandleSearchImageButtonClick(this, sender, e, searchbarEditText.Text);

            InputMethodManager imm = (InputMethodManager)mainActivity.BaseContext.GetSystemService(Context.InputMethodService);
            imm.HideSoftInputFromWindow(searchbarEditText.WindowToken, 0);
        }

        private void mapAddPinFAB_Click(object sender, EventArgs e)
        {
            if (!addPinIsActive)
            {
                addPinIsActive = true;
                Toast.MakeText(Android.App.Application.Context, "click map to add a Pin", ToastLength.Short).Show();
                mapAddPinFAB.Animate()
                    .Rotation(90)
                    .SetDuration(250)
                    .ScaleX(2f)
                    .ScaleY(2f)
                    .Start();
                mapAddPinFAB.SetImageDrawable(ContextCompat.GetDrawable(this.Context, Resource.Drawable.outline_done_24));
                mapAddPinFAB.Animate()
                    .Rotation(360)
                    .SetDuration(250)
                    .ScaleX(1)
                    .ScaleY(1)
                    .Start();
            }
            else if (addPinIsActive)
            {
                if (MapController.HandleAddPinIsActive_FABCLick(this))
                {

                    //temporary implementation, change for PinDetailFragment, open PinDetailFragment here
                    mainActivity.ReplaceContent(this, null, new List<Android.Support.V4.App.Fragment>() { new PinDetail_frgmt() }, "pinDetail");

                    mapAddPinFAB.Animate()
                        .Rotation(90)
                        .SetDuration(250)
                        .ScaleX(2f)
                        .ScaleY(2f)
                        .Start();
                    mapAddPinFAB.SetImageDrawable(ContextCompat.GetDrawable(this.Context, Resource.Drawable.plus));
                    mapAddPinFAB.Animate()
                        .Rotation(360)
                        .SetDuration(250)
                        .ScaleX(1)
                        .ScaleY(1)
                        .Start();
                }
            }
        }

        private async void MapLocationFAB_Click(object sender, EventArgs e)
        {
            if (CrossGeolocator.IsSupported && CrossGeolocator.Current.IsGeolocationAvailable && CrossGeolocator.Current.IsGeolocationEnabled)
            {
                var loc = await CrossGeolocator.Current.GetLastKnownLocationAsync();
                location.Longitude = loc.Longitude;
                location.Latitude = loc.Latitude;
                location.Altitude = loc.Altitude;
                var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(location.Longitude, location.Latitude);
                mapcontrol.Navigator.NavigateTo(sphericalMercatorCoordinate, mapcontrol.Map.Resolutions[14]);
            }
            else
            {
                Toast.MakeText(Application.Context, "Bitte Standortdienste aktivieren", ToastLength.Short).Show();
            }
        }

        public async override void OnDestroyView()
        {
            base.OnDestroyView();
            await StopListening();
        }

        private async void StartLocationListening()
        {
            await StartListening();
        }
        async Task StartListening()
        {
            if (CrossGeolocator.Current.IsListening)
                return;

            await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 10, true);

            CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
            ////CrossGeolocator.Current.PositionError += PositionError;
        }
        async Task StopListening()
        {
            if (!CrossGeolocator.Current.IsListening)
                return;

            await CrossGeolocator.Current.StopListeningAsync();

            CrossGeolocator.Current.PositionChanged -= Current_PositionChanged;
            //CrossGeolocator.Current.PositionError -= PositionError;
        }

        private async void Current_PositionChanged(object sender, PositionEventArgs e)
        {
            var loc = await CrossGeolocator.Current.GetLastKnownLocationAsync();
            location.Longitude = loc.Longitude;
            location.Latitude = loc.Latitude;
            location.Altitude = loc.Altitude;
            //WritableLayer currentLocationLayer = (WritableLayer)mapcontrol.Map.Layers.Where<ILayer>(l => l.Name == "Current Location");
            WritableLayer currentLocationLayer = (WritableLayer)mapcontrol.Map.Layers[1];
            currentLocationLayer.Clear();
            currentLocationLayer.Add(CreateMyLocationSource());
        }

        public void Setup(IMapControl mapControl)
        {
            mapControl.Map = CreateMap();
            mapControl.Info += MapControl_Info;
        }
        private void MapControl_Info(object sender, MapInfoEventArgs e)
        {
            switch (e.NumTaps)
            {
                case 1:
                    if (addPinIsActive)
                    {
                        MapController.HandleAddPinIsActive_MapTap(sender, e ,this);
                        mapcontrol.Map.Layers.Add(CreatePointLayer());
                        mapcontrol.RefreshData();
                        mapcontrol.Refresh();
                        //Toast.MakeText(Android.App.Application.Context, adress, ToastLength.Short).Show();
                    }
                    else
                    {
                        //TODO check if mapsui feature was touched and open corresponding ViewPin
                    }
                    break;
                default:
                    break;
            }

        }


        public Map CreateMap()
        {
            var map = new Map();
            map.RotationLock = true;
            map.Layers.Add(OpenStreetMap.CreateTileLayer());
            map.Layers.Add(CreateCurrentLocationLayer());
            map.Layers.Add(CreatePointLayer());

            //set inital map viewport


            if (location != null)
            {
                var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(location.Longitude, location.Latitude);
                map.Home = n => n.NavigateTo(sphericalMercatorCoordinate, map.Resolutions[9]);
            }

            else
            {
                var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(pinList[0].lon, pinList[0].lat);
                map.Home = n => n.NavigateTo(sphericalMercatorCoordinate, map.Resolutions[6]);
            }


            return map;
        }
        private static async Task<LocationObject> GetLocation()
        {
            try
            {
                var location = await Geolocation.GetLastKnownLocationAsync();

                if (location != null)
                {
                    return new LocationObject(location.Latitude, location.Longitude, location.Altitude);
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
            }
            catch (System.Exception ex)
            {
                // Unable to get location
            }

            return null;
        }

        public MemoryLayer CreatePointLayer()
        {
            return new MemoryLayer
            {
                Name = "Points",
                IsMapInfoLayer = true,
                DataSource = new MemoryProvider(CreatePointSource()),
                Style = CreateBitmapStyle("Schmaps.Resources.drawable.pin.png", 0.5)
            };
        }
        private WritableLayer CreateCurrentLocationLayer()
        {
            WritableLayer currentLocationLayer = new WritableLayer();
            currentLocationLayer.Name = "Current Location";
            currentLocationLayer.IsMapInfoLayer = true;
            currentLocationLayer.Style = CreateBitmapStyle("Schmaps.Resources.drawable.mylocation.png", 0);
            currentLocationLayer.Add(CreateMyLocationSource());

            return currentLocationLayer;
        }

        private IFeature CreateMyLocationSource()
        {
            var feature = new Feature();
            if (location != null)
            {
                var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(location.Longitude, location.Latitude);
                feature.Geometry = sphericalMercatorCoordinate;
                feature["Label"] = "Current Location";
                return feature;
            }
            else
            {
                var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(pinList[0].lon, pinList[0].lat);
                feature.Geometry = sphericalMercatorCoordinate;
                feature["Label"] = "Current Location";
                return feature;
            }

        }

        private IEnumerable<IFeature> CreatePointSource()
        {
            return pinList.Select(c =>
            {
                var feature = new Feature();
                var point = SphericalMercator.FromLonLat(c.lon, c.lat);
                feature.Geometry = point;
                feature["Label"] = c.name;
                return feature;
            });

        }

        private static SymbolStyle CreateBitmapStyle(string path, double offset)
        {
            //var bitmapId = Resource.Drawable.pin;
            //var bitmapHeight = 240; // To set the offset correct we need to know the bitmap height
            //SymbolStyle.DefaultHeight = 240;
            //SymbolStyle.DefaultWidth = 240;
            //return new SymbolStyle { Enabled = true, BitmapId = bitmapId, SymbolScale = 0.20, SymbolOffset = new Offset(0, bitmapHeight) };
            var bitmapId = GetBitmapIdForEmbeddedResource(path);
            var bitmapHeight = 240; // To set the offset correct we need to know the bitmap height
            return new SymbolStyle { BitmapId = bitmapId, SymbolScale = 0.20, SymbolOffset = new Offset(0, bitmapHeight * offset) };
        }
        private static int GetBitmapIdForEmbeddedResource(string imagePath)
        {
            var assembly = typeof(Map_frgmt).GetTypeInfo().Assembly;
            var image = assembly.GetManifestResourceStream(imagePath);
            return BitmapRegistry.Instance.Register(image);
        }
    }
}