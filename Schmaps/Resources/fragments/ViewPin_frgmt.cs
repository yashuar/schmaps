﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Mapsui;
using Mapsui.Layers;
using Mapsui.Projection;
using Mapsui.Providers;
using Mapsui.Styles;
using Mapsui.UI;
using Mapsui.UI.Android;
using Mapsui.Utilities;
using Schmaps.Controller;
using Schmaps.Firebase;
using Schmaps.Models;
using Schmaps.Resources.fragments.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using Xamarin.Essentials;

namespace Schmaps.Resources.fragments
{
    public class ViewPin_frgmt : MySupportFragment
    {
        View viewPinFragment;
        MapControl mapcontrol;
        Mapsui.Map map;
        ImageButton editPin;
        ImageButton sharePin;
        ImageButton openInMaps;
        ImageButton deletePin;
        ImageButton savePin;
        private static LocationObject location;
        string pinName;
        public Pin pin;
        int pinPosition;
        bool isCreator;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            base.mainActivity = ((MainActivity)this.Activity);

            location = new LocationObject(Arguments.GetDouble("lat"), Arguments.GetDouble("lon"), 0);
            isCreator = Arguments.GetBoolean("own", false);
            pinPosition = Arguments.GetInt("position");
            pin = PinController.GetPinForViewPin(this, pinPosition);
            pinName = pin.name;
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            viewPinFragment = inflater.Inflate(Resource.Layout.fragment_viewPin, container, false);
            mapcontrol = viewPinFragment.FindViewById<MapControl>(Resource.Id.viewPinMapControl);
            editPin = viewPinFragment.FindViewById<ImageButton>(Resource.Id.editPinButton);
            sharePin = viewPinFragment.FindViewById<ImageButton>(Resource.Id.sharePinButton);
            openInMaps = viewPinFragment.FindViewById<ImageButton>(Resource.Id.openInGoogleMapsButton);
            deletePin = viewPinFragment.FindViewById<ImageButton>(Resource.Id.deletePinButton);
            savePin = viewPinFragment.FindViewById<ImageButton>(Resource.Id.savePinButton);
            editPin.Click += EditPin_Click;
            savePin.Visibility = ViewStates.Gone;

            if (pin.isUnfinished)
            {
                sharePin.Visibility = ViewStates.Gone;
                editPin.Visibility = ViewStates.Gone;
                deletePin.Visibility = ViewStates.Gone;
                savePin.Visibility = ViewStates.Visible;
                savePin.Click += SavePin_Click;
            }
            else
            {

                if (isCreator == true)
                {
                    sharePin.Click += SharePin_Click;
                }
                else
                {
                    sharePin.Visibility = ViewStates.Gone;
                }
                openInMaps.Click += OpenInMaps_Click;
                deletePin.Click += DeletePin_Click;
            }
            Setup(mapcontrol);
            return viewPinFragment;
        }

        private void SavePin_Click(object sender, EventArgs e)
        {
            PinController.HandleViewPinSaveButtonClick(this, sender, e);
        }

        private void DeletePin_Click(object sender, EventArgs e)
        {
            PinController.HandleViewPinDeleteButtonClick(this, sender, e, pin);
        }

        private void OpenInMaps_Click(object sender, EventArgs e)
        {
            String strUri = "geo:0,0?q=" + location.Latitude.ToString(CultureInfo.CreateSpecificCulture("en-US")) + "," + location.Longitude.ToString(CultureInfo.CreateSpecificCulture("en-US")) + "(" + pinName + ")";
            Intent intent = new Intent(Android.Content.Intent.ActionView, Android.Net.Uri.Parse(strUri));
            //Intent.SetClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            intent.SetPackage("com.google.android.apps.maps");
            intent.AddFlags(ActivityFlags.NewTask);
            Application.Context.StartActivity(intent);
        }

        private void SharePin_Click(object sender, EventArgs e)
        {
            SharePin_frgmt share = new SharePin_frgmt();
            share.Arguments = new Bundle();
            share.Arguments.PutInt("position", pinPosition);
            mainActivity.ReplaceContent(this, null, new List<Android.Support.V4.App.Fragment>() {share}, "sharePin");
        }

        private void EditPin_Click(object sender, EventArgs e)
        {
            PinController.HandleViewPinEditPinButtonClick(this, sender, e);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(location.Longitude, location.Latitude);
            mapcontrol.Navigator.NavigateTo(sphericalMercatorCoordinate, map.Resolutions[18]);
        }
        public void Setup(IMapControl mapControl)
        {
            mapControl.Map = CreateMap();
        }
        public Mapsui.Map CreateMap()
        {
            map = new Mapsui.Map();
            map.Layers.Add(OpenStreetMap.CreateTileLayer());
            map.Layers.Add(CreatePointLayer());

            return map;
        }
        private MemoryLayer CreatePointLayer()
        {
            return new MemoryLayer
            {
                Name = "Points",
                IsMapInfoLayer = true,
                DataSource = new MemoryProvider(CreatePointSource()),
                Style = CreateBitmapStyle("Schmaps.Resources.drawable.pin.png", 0.5)
            };
        }
        private IFeature CreatePointSource()
        {
            var feature = new Feature();
            var point = SphericalMercator.FromLonLat(location.Longitude, location.Latitude);
            feature.Geometry = point;
            return feature;
        }
        private static SymbolStyle CreateBitmapStyle(string path, double offset)
        {
            //var bitmapId = Resource.Drawable.pin;
            //var bitmapHeight = 240; // To set the offset correct we need to know the bitmap height
            //SymbolStyle.DefaultHeight = 240;
            //SymbolStyle.DefaultWidth = 240;
            //return new SymbolStyle { Enabled = true, BitmapId = bitmapId, SymbolScale = 0.20, SymbolOffset = new Offset(0, bitmapHeight) };
            var bitmapId = GetBitmapIdForEmbeddedResource(path);
            var bitmapHeight = 240; // To set the offset correct we need to know the bitmap height
            return new SymbolStyle { BitmapId = bitmapId, SymbolScale = 0.20, SymbolOffset = new Offset(0, bitmapHeight * offset) };
        }
        private static int GetBitmapIdForEmbeddedResource(string imagePath)
        {
            var assembly = typeof(Map_frgmt).GetTypeInfo().Assembly;
            var image = assembly.GetManifestResourceStream(imagePath);
            return BitmapRegistry.Instance.Register(image);
        }
    }
}