﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Schmaps.Controller;
using Schmaps.Firebase;
using Schmaps.Models;
using Schmaps.Resources.fragments.Helpers;

namespace Schmaps.Resources.fragments
{
    public class PinList_frgmt : MySupportFragment
    {
        RecyclerView pinListRecyclerView;
        View pinsFragment;
        public PinAdapter adapter;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            base.mainActivity = ((MainActivity)this.Activity);
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            pinsFragment = inflater.Inflate(Resource.Layout.fragment_pinList, container, false);
            pinListRecyclerView = pinsFragment.FindViewById<RecyclerView>(Resource.Id.PinListRecyclerView);
            if (adapter == null)
            {
                PinController.PrepareExamplePin(this);
            }
            adapter.ItemClick += PinListItemClicked;
            LinearLayoutManager manager = new LinearLayoutManager(pinsFragment.Context);
            RecyclerView.LayoutManager layoutManager = manager;
            pinListRecyclerView.SetLayoutManager(layoutManager);
            pinListRecyclerView.SetAdapter(adapter);
            return pinsFragment;
        }
        private void PinListItemClicked(object sender, int e)
        {
            PinController.HandlePinListItemClicked(this, sender, e);
        }
    }
}